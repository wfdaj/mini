<?php

/**
 * @author  Foma Tuturov <fomiash@yandex.ru>
 * 2023-04-04
 */

namespace core;

class Image
{
    protected $filename;
    protected $image;
    protected $image_type;
    protected $image_format;

    // 通过链接下载源文件
    function load($filename)
    {
        $image_info = getimagesize($filename);
        $this->filename = $filename;
        $this->image_type = $image_info[2];
        $this->image_format = trim(image_type_to_extension($this->image_type), ".");
        if ($this->image_type == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->image_type == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->image_type == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        } elseif ($this->image_type == IMAGETYPE_WEBP) {
            $this->image = imagecreatefromwebp($filename);
        } elseif ($this->image_type == IMAGETYPE_BMP) {
            $this->image = imagecreatefrombmp($filename);
        } elseif ($this->image_type == IMAGETYPE_WBMP) {
            $this->image = imagecreatefromwbmp($filename);
        }
        return empty($this->image) === false;
    }

    // 保存文件
    function save($result_filename, $image_type, $compression = 75)
    {
        if ($image_type == IMAGETYPE_JPEG || strtolower($image_type) == "jpg") {
            return imagejpeg($this->image, $result_filename, $compression);
        } elseif ($image_type == IMAGETYPE_GIF || strtolower($image_type) == "gif") {
            return imagegif($this->image, $result_filename);
        } elseif ($image_type == IMAGETYPE_PNG || strtolower($image_type) == "png") {
            return imagepng($this->image, $result_filename);
        } elseif ($image_type == IMAGETYPE_WEBP || strtolower($image_type) == "webp") {
            return imagewebp($this->image, $result_filename);
        } elseif ($image_type == IMAGETYPE_BMP || strtolower($image_type) == "bmp") {
            return imagebmp($this->image, $result_filename);
        } elseif ($image_type == IMAGETYPE_WBMP || strtolower($image_type) == "wbmp") {
            return imagewbmp($this->image, $result_filename);
        }
        return false;
    }

    // 图像输出到浏览器
    function output($image_type = IMAGETYPE_JPEG)
    {
        if ($image_type == IMAGETYPE_JPEG || strtolower($image_type) == "jpeg" || strtolower($image_type) == "jpg") {
            return imagejpeg($this->image);
        } elseif ($image_type == IMAGETYPE_GIF || strtolower($image_type) == "gif") {
            return imagegif($this->image);
        } elseif ($image_type == IMAGETYPE_PNG || strtolower($image_type) == "png") {
            return imagepng($this->image);
        } elseif ($image_type == IMAGETYPE_WEBP || strtolower($image_type) == "webp") {
            return imagewebp($this->image);
        } elseif ($image_type == IMAGETYPE_BMP || strtolower($image_type) == "bmp") {
            return imagebmp($this->image);
        } elseif ($image_type == IMAGETYPE_WBMP || strtolower($image_type) == "wbmp") {
            return imagewbmp($this->image);
        }
        return false;
    }

    // 图像数据返回
    function getImage()
    {
        return $this->image;
    }

    // 获取原始图像宽度
    function getWidth()
    {
        return imagesx($this->image) ?: 1;
    }

    // 获取原始图像高度
    function getHeight()
    {
        return imagesy($this->image) ?: 1;
    }

    // 获取文件名（不包含后缀名）
    function getFileName()
    {
        $file = pathinfo($this->filename);

        return $file['filename'];
    }

    // 获取文件名（不包含后缀名）
    function getDirName()
    {
        $file = pathinfo($this->filename);

        return $file['dirname'];
    }

    // 获取文件类型
    function getImageType()
    {
        return $this->image_type;
    }

    // 文件扩展名（按类型）
    function getImageFormat()
    {
        if (strtolower($this->image_format) === 'jpeg') {
            $this->image_format = 'jpg';
        }
        return $this->image_format;
    }

    // 正在更新读取的文件
    function getFilePath()
    {
        return $this->filename;
    }

    // 按高度调整大小
    function resizeToHeight($height)
    {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);
    }

    // 按宽度调整大小
    function resizeToWidth($width)
    {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width, $height);
    }

    // 比例变化百分比
    function scale($scale)
    {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }

    // 按指定宽度和高度调整大小
    function resize($width, $height)
    {
        $width = ceil($width);
        $height = ceil($height);
        $new_image = imagecreatetruecolor($width, $height);
        $this->imageCopyResampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }

    // 按比例调整大小，以中心为导向，两侧无空空间（多余）
    function resizeInCenter($width, $height)
    {
        $width = ceil($width);
        $height = ceil($height);
        $new_image = imagecreatetruecolor($width, $height);
        $img_width = $this->getWidth();
        $img_height = $this->getHeight();
        if($img_width == 0 || $img_height == 0) return;
        $new_width = $width;
        $new_height = $height;
        $x = $y = 0;
        $dw = ceil( $width / $img_width);
        $dh = ceil($height / $img_height);
        if ($dw > $dh) {
            $new_height = $img_height * $dw;
            $y = ceil(($height - $new_height) / 2);
        } else if ($dh > $dw) {
            $new_width = $img_width * $dh;
            $x = ceil(($width - $new_width) / 2);
        }
        $this->imageCopyResampled($new_image, $this->image, $x, $y, 0, 0, $new_width, $new_height, $img_width, $img_height);
        $this->image = $new_image;
    }

    // 裁剪指定区域的图像
    function cropBySelectedRegion($width, $height, $x, $y)
    {
        $width = ceil($width);
        $height = ceil($height);
        $x = ceil($x);
        $y = ceil($y);
        $new_image = imagecreatetruecolor($width, $height);
        $img_width = $this->getWidth();
        $img_height = $this->getHeight();
        $this->imageCopyResampled($new_image, $this->image, 0, 0, $x, $y, $img_width, $img_height, $img_width, $img_height);
        $this->image = $new_image;
    }

    // 设置RGB背景（ResizeAllinCenter方法）
    function addRgbColor($red, $green, $blue)
    {
        return [$red, $green, $blue];
    }

    // PNG透明背景上按比例调整大小（所有类型可选着色）
    function resizeAllInCenter($width, $height, $background = null)
    {
        $width = ceil($width);
        $height = ceil($height);
        list($r, $g, $b) = $background ? (is_array($background) ? $background : sscanf($background, "#%02x%02x%02x")) : [0, 0, 0];
        $new_image = imagecreatetruecolor($width, $height);
        $color = imagecolorallocate($new_image, $r, $g, $b);
        if ($background) {
            imagefilledrectangle($new_image, 0, 0, $width, $height, $color);
        } else {
            imagecolortransparent($new_image, $color);
        }
        $img_width = $this->getWidth();
        $img_height = $this->getHeight();
        if($img_width == 0 || $img_height == 0) return;
        $new_width = $width;
        $new_height = $height;
        $x = $y = 0;
        $dw = ceil($width / $img_width);
        $dh = ceil($height / $img_height);
        if ($dw < $dh) {
            $new_height = $img_height * $dw;
            $y = ceil(($height - $new_height) / 2);
        } else if ($dh < $dw) {
            $new_width = $img_width * $dh;
            $x = ceil(($width - $new_width) / 2);
        }
        $this->imageCopyResampled($new_image, $this->image, $x, $y, 0, 0, $new_width, $new_height, $img_width, $img_height);
        $this->image = $new_image;
    }


    protected function imageCopyResampled($dst_image, $src_image, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h)
    {
        imagecopyresampled(
            $dst_image,
            $src_image,
            intval(ceil($dst_x)),
            intval(ceil($dst_y)),
            intval(ceil($src_x)),
            intval(ceil($src_y)),
            intval(ceil($dst_w)),
            intval(ceil($dst_h)),
            intval($src_w),
            intval($src_h)
        );
    }

}
