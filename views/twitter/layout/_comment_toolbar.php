<div class="dropdown">
    <button class="btn btn-sm btn-icon" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="iconfont icon-smile fs-20"></i>
    </button>
    <ul class="dropdown-menu dropdown-menu-star shadow-sm emoji-list p-2" data-stoppropagation="true">
        <li onclick="comment_editor.insert('❤', -1, true);">❤</li>
        <li onclick="comment_editor.insert('❄', -1, true);">❄</li>
        <li onclick="comment_editor.insert('⛄', -1, true);">⛄</li>
        <li onclick="comment_editor.insert('🈚', -1, true);">🈚</li>
        <li onclick="comment_editor.insert('🈶', -1, true);">🈶</li>
        <li onclick="comment_editor.insert('😃', -1, true);">😃</li>
        <li onclick="comment_editor.insert('😅', -1, true);">😅</li>
        <li onclick="comment_editor.insert('😉', -1, true);">😉</li>
        <li onclick="comment_editor.insert('😍', -1, true);">😍</li>
        <li onclick="comment_editor.insert('😝', -1, true);">😝</li>
        <li onclick="comment_editor.insert('😏', -1, true);">😏</li>
        <li onclick="comment_editor.insert('😒', -1, true);">😒</li>
        <li onclick="comment_editor.insert('😞', -1, true);">😞</li>
        <li onclick="comment_editor.insert('😔', -1, true);">😔</li>
        <li onclick="comment_editor.insert('😓', -1, true);">😓</li>
        <li onclick="comment_editor.insert('💩', -1, true);">💩</li>
        <li onclick="comment_editor.insert('👐', -1, true);">👐</li>
        <li onclick="comment_editor.insert('👊', -1, true);">👊</li>
        <li onclick="comment_editor.insert('🙈', -1, true);">🙈</li>
        <li onclick="comment_editor.insert('⚽', -1, true);">⚽</li>
    </ul>
</div>

<script>
    let comment_editor;
    document.addEventListener('DOMContentLoaded', () => {
        const textareas = document.querySelectorAll('textarea');
        if (textareas.length > 1) {
            // 评论框编辑器
            comment_editor = new TextEditor(textareas[1]);
        }
    });
</script>