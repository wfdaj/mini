{if !session('user_id') }
<!-- 提醒登录 -->
<a href="javascript:;" class="post-action-fav js-jump" data-bs-toggle="tooltip" data-bs-trigger="hover" title="收藏">
    <div class="btn btn-sm btn-icon post-action-icon">
        <i class="iconfont icon-fav js-no-click"></i>
    </div>
    <span class="fs-13">{$post->favorites? $post->favorites : ''}</span>
</a>
{/if}

{if session('user_id') }
    {if ($post->favorites)}
    <!-- 取消收藏 -->
    <a href="javascript:;" class="post-action-fav favorited js-fav" data-pid="{$post->id}" data-bs-toggle="tooltip" data-bs-trigger="hover" title="取消收藏">
        <div class="btn btn-sm btn-icon post-action-icon">
            <i class="iconfont icon-fav-fill js-no-click"></i>
        </div>
        <span class="fs-13 js-favNum{$post->id}">{$post->favorites}</span>
    </a>
    {else/}
        <!-- 点击收藏 -->
        <a href="javascript:;" class="post-action-fav js-fav" data-pid="{$post->id}" data-bs-toggle="tooltip" data-bs-trigger="hover" title="收藏">
            <div class="btn btn-sm btn-icon post-action-icon">
                <i class="iconfont icon-fav js-no-click"></i>
            </div>
            <span class="fs-13 js-favNum{$post->id}"></span>
        </a>
    {/if}
{/if}
