{if session('user_id')}
<div class="only-on-sm" data-bs-toggle="offcanvas" href="#offcanvasNav" role="button" aria-controls="offcanvasNav">
    <img class="avatar" src="<?= getAvatar(session('user_id')); ?>" alt="">
</div>
{else /}
<a href="/login" class="btn btn-sm btn-icon only-on-sm" role="button">
    <i class="iconfont icon-user"></i>
</a>
{/if}
