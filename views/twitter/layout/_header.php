<header id="app-header" class="flex-column align-items-end" role="banner">
    <div class="header-inner d-flex flex-column align-items-end">
        <div class="app-header d-flex flex-column">
            <div class="d-flex flex-column">
                <h1 class="app-logo" role="heading">
                    <a href="/"><img src="/img/logo.png" alt=""></a>
                </h1>
                <nav class="app-nav d-flex flex-column" role="navigation">
                    <a href="/" class="app-link" data-active="home" title="主页">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-home"></i>
                            <div class="app-nav-link px-3 fs-20">主页</div>
                            <div class="unread-dot"></div>
                        </div>
                    </a>
                    <a class="app-link" href="/explore" data-active="explore" title="探索">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-search"></i>
                            <div class="app-nav-link px-3 fs-20">探索</div>
                        </div>
                    </a>
                    <a href="/tag" class="app-link" data-active="tag" title="标签">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-explore"></i>
                            <div class="app-nav-link px-3 fs-20">标签</div>
                        </div>
                    </a>
                    <a class="app-link" href="/reward" data-active="reward" title="奖励">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-present"></i>
                            <div class="app-nav-link px-3 fs-20">领金币</div>
                        </div>
                    </a>
                    <a class="app-link" href="/medal" data-active="medal" title="勋章">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-medal"></i>
                            <div class="app-nav-link px-3 fs-20">勋章</div>
                        </div>
                    </a>
                    <a class="app-link" href="/tool" data-active="tool" title="工具箱">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-work"></i>
                            <div class="app-nav-link px-3 fs-20">工具箱</div>
                        </div>
                    </a>
                    <!-- <a class="app-link" href="/lists" data-active="lists" title="列表">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-lists"></i>
                            <div class="app-nav-link px-3 fs-20">列表</div>
                        </div>
                    </a>
                    <a class="app-link" href="/message" data-active="message" title="消息">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-notifications"></i>
                            <div class="app-nav-link px-3 fs-20">消息</div>
                            <div class="unread-message">1</div>
                        </div>
                    </a>
                    <a class="app-link" href="/post" data-active="post" title="探索">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-topics"></i>
                            <div class="app-nav-link px-3 fs-20">动态</div>
                        </div>
                    </a>
                    <a href="/medal" class="app-link" data-active="medal" title="勋章">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-medal"></i>
                            <div class="app-nav-link px-3 fs-20">勋章</div>
                        </div>
                    </a> -->
                    {if session('user_id')}
                    <a class="app-link" href="/user/profile/<?= session('user_id'); ?>" data-active="user" title="个人资料">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-user"></i>
                            <div class="app-nav-link px-3 fs-20">个人资料</div>
                        </div>
                    </a>
                    <button type="button" id="createPost" class="btn btn-primary justify-content-center rounded-pill" data-bs-toggle="modal" data-bs-target="#js-createPostModal">发帖</button>
                    <!-- 小屏显示的发帖按钮 -->
                    <button type="button" class="btn btn-primary twteet-btn" data-bs-toggle="modal" data-bs-target="#js-createPostModal" title="发帖">
                        <div class="nav-item d-inline-flex justify-content-center">
                            <i class="iconfont icon-feather"></i>
                        </div>
                    </button>
                    {else /}
                    <a class="app-link" href="/login" data-active="user" title="登录">
                        <div class="nav-item d-inline-flex">
                            <i class="iconfont icon-user"></i>
                            <div class="app-nav-link px-3 fs-20">登录</div>
                        </div>
                    </a>
                    {/if}
                </nav>
            </div>
            {if session('user_id')}
            <div class="dropup mt-auto">
                <div class="d-flex align-items-center fs-15 account-switcher-btn" id="account-switcher-btn" data-bs-toggle="dropdown" aria-expanded="false">
                    <img class="avatar" src="<?= getAvatar(session('user_id')); ?>" alt="">
                    <div class="d-flex flex-column lh-1">
                        <div><?= session('username'); ?></div>
                    </div>
                    <div class="ms-auto">
                        <i class="iconfont icon-more fs-20"></i>
                    </div>
                </div>
                <div class="dropdown-menu shadow-sm">
                    {if ( isAdmin() )}
                    <a href="/admin" class="dropdown-item">
                        <i class="iconfont icon-settings fs-20 me-2"></i>
                        <span class="fw-bold">后台</span>
                    </a>
                    {/if}
                    <a href="/auth/logout" class="dropdown-item">
                        <i class="iconfont icon-logout fs-20 me-2"></i>
                        <span class="fw-bold">退出 <?= session('username'); ?></span>
                    </a>
                </div>
            </div>
            {/if}
        </div>
    </div>
</header>

<!-- 弹出的发帖层 -->
<div class="modal fade" id="js-createPostModal" tabindex="-1" aria-labelledby="js-createPostModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-18" id="js-createPostModalLabel">发新帖</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                {include file="layout/_composer" /}
            </div>
        </div>
    </div>
</div>
