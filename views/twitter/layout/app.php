<!doctype html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/font/iconfont.css">
    <link rel="stylesheet" href="/css/mini.css">
    <title>{block name="title"}{/block} | <?= config('app.name'); ?></title>
</head>

<body>
    <div class="app d-flex">
        {include file="layout/_header" /}
        <main id="app-main" role="main">
            <div class="main-inner d-flex justify-content-between">
                <div class="main-middle">
                    {block name="main"}{/block}

                </div>
                <aside id="app-aside">
                    {block name="aside"}{/block}
                </aside>
            </div>
        </main>
    </div>

    {if session('user_id')}
    <!-- 小屏幕发帖按钮 -->
    <div class="btn btn-primary btn-icon only-on-sm float-post-btn rounded-pill bg-primary" data-bs-toggle="modal" data-bs-target="#js-createPostModal">
        <i class="iconfont icon-feather fs-20 text-white"></i>
    </div>
    {/if}

    <!-- 登录后，点击头像弹出的导航 -->
    {if session('user_id')}
    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasNav" aria-labelledby="offcanvasNavLabel">
        <div class="offcanvas-header">
            <img class="avatar" src="<?= getAvatar(session('user_id')); ?>" alt="">
            <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="px-3">
            <strong><?= session('username'); ?></strong>
            <p class="fs-14 text-muted">正在关注 <span class="ms-2">关注者</span></p>
        </div>
        <div class="offcanvas-body">
            <nav class="app-nav d-flex flex-column" role="navigation">
                <a href="/" class="app-link" data-active="home" title="主页">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-home"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">主页</div>
                        <div class="unread-dot"></div>
                    </div>
                </a>
                <a class="app-link" href="/explore" data-active="explore" title="探索">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-search"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">探索</div>
                    </div>
                </a>
                <a class="app-link" href="/reward" data-active="reward" title="奖励">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-present"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">领金币</div>
                    </div>
                </a>
                <a class="app-link" href="/medal" data-active="medal" title="勋章">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-medal"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">勋章</div>
                    </div>
                </a>
                <a class="app-link" href="/tool" data-active="tool" title="工具箱">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-work"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">工具箱</div>
                    </div>
                </a>
                <a class="app-link" href="/user/profile/<?= session('user_id'); ?>" data-active="user" title="个人资料">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-user"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">个人资料</div>
                    </div>
                </a>
                <a class="app-link" href="/admin">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-settings"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">后台</div>
                    </div>
                </a>
                <a class="app-link" href="/auth/logout" class="dropdown-item">
                    <div class="nav-item d-inline-flex">
                        <i class="iconfont icon-logout"></i>
                        <div class="app-nav-link d-inline px-3 fs-20">退出</div>
                    </div>
                </a>
            </nav>
        </div>
    </div>
    {/if}
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap5.bundle.min.js"></script>
    <script src="/js/editor.min.js"></script>
    <script src="/js/mini.js"></script>
    <script src="/js/gifffer.js"></script>
    {block name="js"}{/block}
</body>

</html>