<?php
    use \app\controllers\Comment;
    $postId = segment(2);
    $comments = Comment::list($postId);
    $floors = $comments[1]->totalRows;
?>

<?php if($comments[1]->totalRows > 0): ?>
    <ul class="list-unstyled chat p-3 border-bottom">
    <?php foreach ($comments[0] as $comment): ?>
        <?php if( !isAuthor($comment->user_id, session('user_id')) ): ?>
        <li class="others">
            <div class="chat-item">
                <a href="/user/profile/<?= $comment->user_id ?>">
                    <img class="avatar me-2" src="<?= getAvatar($comment->user_id) ?>" alt="">
                </a>
                <div>
                    <div class="chat-content">
                        <?= $comment->content ?>
                    </div>
                    <span class="fs-14 text-muted"><?= $comment->username ?> · <?= nice_time($comment->created_at) ?></span>
                </div>
            </div>
        </li>
        <?php else: ?>
        <li class="self">
            <div class="chat-item">
                <a href="/user/profile/<?= $comment->user_id ?>">
                    <img class="avatar ms-2" src="<?= getAvatar($comment->user_id) ?>" alt="">
                </a>
                <div>
                    <div class="chat-content">
                        <?= $comment->content ?>
                    </div>
                    <span class="fs-14 text-muted"><?= nice_time($comment->created_at) ?> · <?= $comment->username ?></span>
                </div>
            </div>
        </li>
        <?php endif ?>
    <?php endforeach ?>
    </ul>
<?php else: ?>
    <div class="placeholder fs-18">暂无评论</div>
<?php endif ?>