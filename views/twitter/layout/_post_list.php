<ul class="feed-list">
{if !$posts[0] }
    <div class="placeholder fs-28">無</div>
{else /}
    {foreach $posts[0] as $post }
    <li class="feed-item d-flex js-tap" id="feed{$post->id}" data-href="/post/show/{$post->id}" data-pid="{$post->id}">
        <!-- <a href="/user/profile/{$post->user_id}" data-letters="{$post->username | mb_substr=0,1}"></a> -->
        <a href="/user/profile/{$post->user_id}" class="position-relative">
            <img class="avatar" src="<?= getAvatar($post->user_id); ?>" alt="">
            {if $post->is_sticky}
            <div class="avatar-badge bg-danger">
                <i class="iconfont icon-thumbtack text-white fs-12"></i>
            </div>
            {/if}
        </a>
        <div class="d-flex flex-column w-100">
            <div class="d-flex align-items-center fs-15">
                <a class="link-dark fw-bold post-meta" href="/user/profile/{$post->user_id}">{$post->username}</a>
                <a class="link-secondary" href="/post/show/{$post->id}">{$post->created_at | nice_time}</a>
            </div>
            <p class="typo-text mt-1">{$post->content|raw}</p>
            {if $post->images > 0}
                {php} $post->images = min(max(1, $post->images), 3); {/php}
                <div class="gallery mt-2">
                    <ul class="thumbnail-container feed-item-imgs list-unstyled row row-cols-{$post->images} mx-0">{php} echo getImagesList($post->images, $post->id); {/php}</ul>
                    <div class="fullscreen-container">
                        <div class="loading-indicator"><div class="spinner-border m-4" role="status"></div></div>
                        <div class="image-wrapper">
                            <img class="fullscreen img-fluid rounded js-no-click" src="" alt="">
                            <i class="iconfont icon-return prev-btn text-white js-no-click" title="上一张"></i>
                            <i class="iconfont icon-enter next-btn text-white js-no-click" title="下一张"></i>
                        </div>
                    </div>
                </div>
            {/if}
            <div class="mt-1 d-flex justify-content-between post-actions">
                <!-- 评论 -->
                <a role="button" class="post-action-reply cursor-arrow" data-bs-toggle="tooltip" data-bs-trigger="hover" title="评论">
                    <div class="btn btn-sm btn-icon post-action-icon">
                        <i class="iconfont icon-comment js-no-click"></i>
                    </div>
                    <span class="fs-13">{$post->comments? $post->comments : ''}</span>
                </a>

                <!-- 转帖 -->
                <a href="javascript:;" class="post-action-retweet cursor-default" data-bs-toggle="tooltip" data-bs-trigger="hover" title="转帖">
                    <div class="btn btn-sm btn-icon post-action-icon">
                        <i class="iconfont icon-retweet js-no-click"></i>
                    </div>
                </a>

                <!-- 喜欢 -->
                {include file="layout/_like" /}

                <!-- 收藏 -->
                {include file="layout/_fav" /}
            </div>
        </div>
    </li>
    {/foreach}
{/if}
</ul>

<script>
document.addEventListener('DOMContentLoaded', function() {
    function initGallery(galleryElement) {
        const thumbnailContainer = galleryElement.querySelector('.thumbnail-container');
        const fullscreenContainer = galleryElement.querySelector('.fullscreen-container');
        const fullscreenImg = fullscreenContainer.querySelector('.fullscreen');
        const prevBtn = fullscreenContainer.querySelector('.prev-btn');
        const nextBtn = fullscreenContainer.querySelector('.next-btn');
        const thumbnails = Array.from(thumbnailContainer.querySelectorAll('.feed-item-img'));

        // 根据缩略图数量决定是否显示上一张和下一张按钮
        if (thumbnails.length <= 1) {
            prevBtn.style.display = 'none';
            nextBtn.style.display = 'none';
        }

        let currentIndex = 0;

        function showFullscreenImage() {
            const clickedThumbnail = thumbnails[currentIndex];
            let fullImageUrl = clickedThumbnail.getAttribute('data-full');

            // 添加加载状态
            fullscreenContainer.style.display = 'flex';
            fullscreenContainer.classList.add('loading');

            // 使用 onload 事件来确保图片加载完成后再更新显示
            const img = new Image();
            img.onload = function() {
                fullscreenImg.src = this.src; // 现在图片已经加载，可以安全地设置到全屏 img 元素

                // 移除加载状态并显示图片
                fullscreenContainer.classList.remove('loading');
            };
            img.onerror = function() {
                // 处理图片加载错误的情况
                // ...
                // 移除加载状态，并可能显示一个错误消息
                fullscreenContainer.classList.remove('loading');
            };
            img.src = fullImageUrl; // 设置图片的源以开始加载
        }

        // 为缩略图添加点击事件监听
        thumbnails.forEach(thumbnail => {
            thumbnail.addEventListener('click', () => {
                thumbnailContainer.style.display = 'none';
                // 移除其他缩略图的高亮
                thumbnails.forEach(thumb => thumb.classList.remove('selected'));
                // 为点击的缩略图添加高亮
                thumbnail.classList.add('selected');

                currentIndex = thumbnails.indexOf(thumbnail);
                showFullscreenImage();
            });
        });

        // 为大图添加点击事件监听
        fullscreenImg.addEventListener('click', () => {
            fullscreenContainer.style.display = 'none';
            thumbnailContainer.style.display = 'flex';
            // 可选：移除缩略图的高亮
            thumbnails.forEach(thumb => thumb.classList.remove('selected'));
        });

        // 如果有多于一张的图片，才为按钮添加点击事件
        if (thumbnails.length > 1) {
            prevBtn.addEventListener('click', () => {
                currentIndex = (currentIndex - 1 + thumbnails.length) % thumbnails.length;
                showFullscreenImage();
            });

            nextBtn.addEventListener('click', () => {
                currentIndex = (currentIndex + 1) % thumbnails.length;
                showFullscreenImage();
            });
        }
    }

    // 初始化所有的图片列表
    const galleries = document.querySelectorAll('.gallery');
    galleries.forEach(gallery => initGallery(gallery));
});
</script>