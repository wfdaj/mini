<?php if (session('user_id')) : ?>
    <div class="composer d-flex px-3 border-bottom js-elastic" id="jsCommentTextarea">
        <img class="avatar" src="<?= getAvatar(session('user_id')) ?>" alt="">
        <div class="flex-fill">
            <form id="commentForm" method="POST" action="/comment/submit">
                <input type="hidden" name="csrf_token" value="<?= csrf_token() ?>">
                <input type="hidden" name="post_id" id="post_id" value="<?= $post->id ?>">
                <textarea class="form-control p-3 mb-2 bg-light border-0 js-textarea" name="commentTextarea" id="commentTextarea" maxlength="200" spellcheck="false" placeholder="说说您的想法" required></textarea>
                <div class="composer-action d-flex align-items-center mb-2">
                {include file="layout/_comment_toolbar" /}
                {if !isset($user)}
                    <a href="/login" role="button" class="btn btn-primary ms-2 fw-bold">请先登录</button>
                {/if}
                {if isset($user) && $user !== null}
                    {if isset($user->golds) && $user->golds <= 0}
                    <button type="button" class="btn btn-primary ms-2 fw-bold" disabled>金币不足</button>
                    {else /}
                    <button type="button" id="submitComments" class="btn btn-sm btn-primary rounded-pill ms-auto submitButton" disabled>评论</button>
                    <!-- <button type="submit" class="btn btn-primary rounded-pill fw-bold submitButton" disabled>回帖</button> -->
                    {/if}
                {/if}
                </div>
            </form>
            <!-- <div>
                <img class="previewImg img-fluid rounded mb-2" id="previewImg">
            </div> -->
        </div>
    </div>
<?php else : ?>
    <div class="p-4 text-center bg-light border-bottom">
        <h3 class="fs-18 fw-bold mb-2">您好！喜欢交流可以注册账号。</h3>
        <p class="fs-15 mb-3 text-muted">使用帐户，您可以回复用户的帖子。您还可以收到新回复通知、收藏帖子，以及使用“喜欢”来感谢他人。</p>
        <a href="/login" class="btn btn-primary rounded-pill">登录</a>
        <a href="/register" class="btn btn-light rounded-pill ms-1">注册</a>
    </div>
<?php endif ?>

<script>
document.addEventListener('DOMContentLoaded', function() {
    // 点击提交按钮后验证
    const submitCommentsElement = document.getElementById('submitComments');
    const commentTextareaElement = document.getElementById('commentTextarea');
    if (submitCommentsElement) {
        submitCommentsElement.addEventListener('click', function() {
            let form = document.getElementById('commentForm');
            let data = new FormData(form);

            // 发送 AJAX 请求
            fetch('/comment/submit', {
                method: 'POST',
                body: data,
            })
            .then(response => response.json())
            .then(res => {
                if (res.status === 'success') {
                    // 显示成功消息
                    toast.success(res.message);
                    setTimeout(() => {
                        location.reload();
                    }, 1500);
                } else {
                    // 显示错误消息
                    toast.error(res.message, 3000);
                }
            })
            .catch(error => {
                console.error('Error:');
            });
        });
    }
});
</script>