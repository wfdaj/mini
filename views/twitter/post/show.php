{extend name="layout/app" /}

{block name="title"}帖子{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-sm btn-icon me-3 js-back" aria-label="返回">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="w-100 fs-18 cursor-pointer js-top"><span class="text-truncate">帖子</span></h2>
</div>

<article class="p-3">
    <div class="d-flex align-items-center">
        <a href="/user/profile/{$post->user_id}" class="position-relative">
            <img class="avatar me-2" src="<?= getAvatar($post->user_id); ?>" alt="">
            {if $post->is_sticky}
            <div class="avatar-badge bg-danger">
                <i class="iconfont icon-thumbtack text-white fs-12"></i>
            </div>
            {/if}
        </a>
        <p class="fs-15"><b>{$post->username}</b></p>
        {if $user}
        <div class="dropdown ms-auto">
            <button class="btn btn-sm btn-icon" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="iconfont icon-more fs-20"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-end shadow-sm">
                <!-- <a class="dropdown-item" href="#">
                    <i class="iconfont icon-shield me-2"></i>
                    <span class="fw-bold">屏蔽</span>
                </a> -->
                <a class="dropdown-item" href="#">
                    <i class="iconfont icon-report me-2"></i>
                    <span class="fw-bold">举报 帖子</span>
                </a>
                {if isAdmin()}
                <a href="javascript:;" class="dropdown-item" id="js-pin" role="button" data-post-id="{$post->id}">
                    <i class="iconfont icon-thumbtack me-2"></i>
                    {if $post->is_sticky}
                    <span class="fw-bold">取消置顶</span>
                    {else /}
                    <span class="fw-bold">置顶</span>
                    {/if}
                </a>
                {/if}
                {if isAuthor($post->user_id, $user->id) || isAdmin()}
                    <a href="javascript:;" class="dropdown-item text-danger" role="button" data-bs-toggle="modal" data-bs-target="#confirmBackdrop">
                        <i class="iconfont icon-trash me-2"></i>
                        <span class="fw-bold">删除</span>
                    </a>
                {/if}
            </div>
        </div>
        {/if}
    </div>
    <div class="typo-text py-3 fs-18" id="postContent"><?= $post->content ?></div>
    {if $post->images > 0}
    <?php $post->images = min(max(1, $post->images), 3); ?>
    <div class="gallery mt-2">
        <ul class="thumbnail-container list-unstyled mx-0"><?= getImagesList($post->images, $post->id, false) ?></ul>
    </div>
    {/if}
    <div class="py-3 fs-15 border-bottom">
        <a href="/post/show/{$post->id}" class="link-secondary">
            <time datetime="<?= date("Y-m-d H:i:s", $post->created_at) ?>"><?= convertTimeToAMPM($post->created_at); ?></time>
        </a>
    </div>
    <div class="py-2 d-flex justify-content-between post-actions ms-0 border-bottom">
        <!-- 评论数 -->
        <a role="button" class="post-action-reply cursor-arrow" data-bs-toggle="tooltip" data-bs-trigger="hover" title="评论">
            <div class="d-flex justify-content-center post-action-icon">
                <i class="iconfont icon-comment"></i>
            </div>
            <span>{$post->comments? $post->comments : ''}</span>
        </a>

        <!-- 点击数 -->
        <a href="javascript:;" class="post-action-fav" data-bs-toggle="tooltip" data-bs-trigger="hover" title="转帖">
            <div class="d-flex justify-content-center post-action-icon">
                <i class="iconfont icon-retweet"></i>
            </div>
        </a>

        <!-- 喜欢 -->
        {include file="layout/_like" /}

        <!-- 收藏 -->
        {include file="layout/_fav" /}

        <!-- 分享 -->
        <a href="javascript:;" class="post-action-fav" data-bs-toggle="tooltip" data-bs-trigger="hover" title="分享">
            <div class="d-flex justify-content-center post-action-icon">
                <i class="iconfont icon-upload"></i>
            </div>
        </a>
    </div>
</article>

{include file="layout/_comment" /}

{include file="layout/_comment_list" /}


<!-- 头像挂件 -->
<!-- <div class="avatar avatar-pendant">
    <img src="/img/avatar_default.jpeg" data="face" alt="">
    <img src="/img/face/garb/fighters.png" data="pendant" alt="">
</div> -->

<!-- 确认删除弹框 -->
<div class="modal fade" id="confirmBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="confirmBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="confirmBackdropLabel">删除帖子</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">要删除 <b>“ {$post->content | mb_substr=0,20}... ”</b> 吗？</div>
            <div class="modal-footer">
                <a role="button" class="btn btn-danger" id="jsDelBtn" data-post-id="{$post->id}">确认删除</a>
            </div>
        </div>
    </div>
</div>
{/block}

{block name="js"}
<script src="/js/post.js"></script>
<script src="/js/medium-zoom.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        mediumZoom('[data-zoomable]', {
            margin: 30,
            background: 'rgba(0,0,0,.7)',
        });
    });
</script>
{/block}