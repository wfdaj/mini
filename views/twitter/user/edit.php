{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon btn-sm me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

<nav class="nav nav-justified border-bottom">
  <a class="nav-link" id="my-info" href="/user/edit/<?= $user->id ?>">账号信息</a>
  <a class="nav-link" id="my-password" href="/user/password/<?= $user->id ?>">更改密码</a>
  <a class="nav-link" id="my-avatar" href="/user/avatar/<?= $user->id ?>">设置头像</a>
</nav>

<div class="container p-4">
    <div class="row lh-lg">
        <div class="col-4 text-muted text-end">用户名</div>
        <div class="col-8"><?= $user->username ?></div>
        <div class="col-4 text-muted text-end">注册时间</div>
        <div class="col-8"><?= date("Y-m-d", $user->created_at) ?></div>
        <div class="col-4 text-muted text-end">最后登录</div>
        <div class="col-8"><?= date("Y-m-d", $user->updated_at) ?></div>

        <hr class="my-3">

        <div class="col-4 text-muted text-end">主题数</div>
        <div class="col-8"></div>
        <div class="col-4 text-muted text-end">回复数</div>
        <div class="col-8"></div>
        <div class="col-4 text-muted text-end">金币数</div>
        <div class="col-8"><?= $user->golds ?></div>

        <hr class="my-3">

        <div class="col-4 text-muted text-end">用户组</div>
        <div class="col-8"></div>
        <div class="col-4 text-muted text-end">邮箱</div>
        <div class="col-8"></div>
    </div>
</div>
{/block}

{block name="js"}

<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-info").classList.add('active');
    });
</script>
{/block}