{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon btn-sm me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

{include file="user/_user_cover_nav"}

<!-- 无帖子提示 -->
{if !$favorites[0]}
    <div class="placeholder fs-28">无</div>
{/if}

<ul class="feed-list list-unstyled">
    {foreach $favorites[0] as $fav}
    <li class="feed-item d-flex p-3 js-tap" data-href="/post/show/{$fav->post_id}" data-pid="{$fav->post_id}">
        <a class="avatar" href="/user/profile/{$fav->author_id}" data-letters="{$fav->username | mb_substr=0,2}"></a>
        <div class="d-flex flex-column w-100">
            <div class="d-flex align-items-center fs-15">
                <a class="link-dark fw-bold" href="/user/profile/{$fav->author_id}">{$fav->username}</a>
                <a class="link-secondary ms-auto" href="/post/show/{$fav->post_id}">{$fav->created_at | nice_time}</a>
            </div>
            <p class="typo-text">{$fav->content }</p>
            {if $fav->images > 0}
                {if $fav->type === 'gif'}
                    <img class="feed-item-img align-self-baseline mt-2" data-gifffer="/upload/{$fav->filename }" alt="" loading="lazy" decoding="async">
                {else /}
                    <img class="feed-item-img align-self-baseline mt-2" src="/{$fav->filename | thumb}" alt="" data-zoomable loading="lazy" decoding="async">
                {/if}
            {/if}
        </div>
    </li>
    {/foreach}
</ul>

<!-- 分页 -->
{php}echo pageLinks($favorites);{/php}

{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-fav").classList.add('active');
    });
</script>
{/block}