{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<!-- 顶部导航 -->
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon btn-sm me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

<nav class="nav nav-justified border-bottom">
  <a class="nav-link" id="my-info" href="/user/edit/<?= $user->id ?>">账号信息</a>
  <a class="nav-link" id="my-password" href="/user/password/<?= $user->id ?>">更改密码</a>
  <a class="nav-link" id="my-avatar" href="/user/avatar/<?= $user->id ?>">设置头像</a>
</nav>

<div class="auth-form mx-auto p-4">
    <form id="auth-form" class="needs-validation" method="POST" autocomplete="off">
        <input type="hidden" name="csrf_token" value="<?= csrf_token() ?>">
        <div class="form-floating mb-3">
            <input type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="旧密码" autocomplete="off" required autofocus>
            <label for="oldPassword" id="oldPasswordHelp">旧密码</label>
        </div>
        <div class="form-floating mb-3">
            <input type="password" class="form-control" name="newPassword1" id="newPassword1" placeholder="新密码" required>
            <label for="newPassword1" id="newPassword1Help">新密码</label>
        </div>
        <div class="form-floating mb-3">
            <input type="password" class="form-control" name="newPassword2" id="newPassword2" placeholder="重复新密码" required>
            <label for="newPassword2" id="newPassword2Help">重复新密码</label>
        </div>
        <button type="button" id="changePasswordBtn" class="btn btn-secondary w-100 p-3">修改密码</button>
        <!-- <button type="submit" class="btn btn-secondary w-100 auth-btn px-0">修改密码</button> -->
    </form>
</div>
{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", () => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-password").classList.add('active');

        const submitBtn = $('#changePasswordBtn');

        let flagoldPassword  = false;
        let flagnewPassword1 = false;
        let flagnewPassword2 = false;

        // 检查旧密码
        $('#oldPassword').change(function() {
            let oldPassword = $('#oldPassword').val().trim();
            if (getStringLength(oldPassword) < 6) {
                $('#oldPassword').addClass('is-invalid');
                $('#oldPasswordHelp').text('密码至少 6 位数').addClass('red');
                flagoldPassword = false;
            } else {
                $('#oldPassword').removeClass('is-invalid');
                $('#oldPasswordHelp').text('旧密码').removeClass('red');
                flagoldPassword = true;
            }
        });
        // 检查新密码
        $('#newPassword1').change(function() {
            let newPassword1 = $('#newPassword1').val().trim();
            if (newPassword1.length < 6) {
                $('#newPassword1').addClass('is-invalid');
                $('#newPassword1Help').text('新密码至少 6 位数').addClass('red');
                flagnewPassword1 = false;
            } else {
                $('#newPassword1').removeClass('is-invalid');
                $('#newPassword1Help').text('新密码').removeClass('red');
                flagnewPassword1 = true;
            }
        });
        // 检查重复密码
        $('#newPassword2').change(function() {
            newPassword2 = $('#newPassword2').val().trim();
            if (newPassword2.length < 6) {
                $('#newPassword2').addClass('is-invalid');
                $('#newPassword2Help').text('重复密码至少 6 位数').addClass('red');
                flagnewPassword2 = false;
            } else {
                $('#newPassword2').removeClass('is-invalid');
                $('#newPassword2Help').text('重复新密码').removeClass('red');
                flagnewPassword2 = true;
            }
        });

        // 点击修改密码按钮后验证
        submitBtn.click(function() {
            if (!flagoldPassword) {
                $('#oldPassword').addClass('is-invalid').focus();
                $('#oldPasswordHelp').text('请输入旧密码');
                return;
            }
            if (!flagnewPassword1) {
                $('#newPassword1').addClass('is-invalid').focus();
                $('#newPassword1Help').text('密码至少 6 位数');
                return;
            }
            if (!flagnewPassword2) {
                $('#newPassword2').addClass('is-invalid').focus();
                $('#newPassword2Help').text('密码至少 6 位数');
                return;
            }

            let data = new FormData(document.getElementById('auth-form'));
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            $.ajax({
                method: 'POST',
                url: '/auth/password',
                data: data,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(res) {
                    submitBtn.prop('disabled', false); // 重新启用提交按钮
                    if (res.status === 'success') {
                        toast.success(res.message);
                        setTimeout(() => { window.location.reload(); }, 2000);
                    } else {
                        toast.error(res.message);
                    }
                },
                error: function(xhr) {
                    submitBtn.prop('disabled', false); // 重新启用提交按钮
                    toast.error('发生错误，请稍后再试。');
                    // console.error(xhr); // 在控制台中记录错误以供调试
                }
            });
        });
    });
</script>
{/block}