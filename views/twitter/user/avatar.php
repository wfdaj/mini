{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon btn-sm me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

<nav class="nav nav-justified border-bottom">
  <a class="nav-link" id="my-info" href="/user/edit/<?= $user->id ?>">账号信息</a>
  <a class="nav-link" id="my-password" href="/user/password/<?= $user->id ?>">更改密码</a>
  <a class="nav-link" id="my-avatar" href="/user/avatar/<?= $user->id ?>">设置头像</a>
</nav>

<div class="auth-form mx-auto p-4 text-center">
    <form class="needs-validation" action="/auth/photo" id="avatar-form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="csrf_token" value="<?= csrf_token() ?>">
        <p class="mb-3">
            <img class="rounded-circle" id="previewImg" src="/img/avatar.jpg" alt="" style="width:120; height:120">
        </p>
        <input class="d-none" type="file" name="userAvatar" id="userAvatar" accept="image/png,image/jpeg,image/gif">
        <button type="button" id="submitBtn" class="btn btn-secondary">
            <label for="userAvatar" class="cursor-pointer">选择图片</label>
        </button>
    </form>
</div>
{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-avatar").classList.add('active');
    });
</script>
{/block}