<div class="position-relative">
    <div class="profile-header-cover">
        <img src="/img/bg-header-cover.jpg" alt="">
    </div>
    <div class=" px-3 d-flex justify-content-between">
        <div class="profile-header-avatar">
            <form id="avatar-form" method="post" enctype="multipart/form-data">
                <img class="avatar" id="previewImg" src="<?= getAvatar($user->id) ?>" alt="">
                <!-- <img class="avatar" id="previewImg" src="/img/avatar.jpg" alt=""> -->
                <div class="upload-avatar d-flex flex-column justify-content-center align-items-center text-white" title="上传新头像">
                    <label for="avatarInput">
                        <input type="file" class="d-none" id="avatarInput" name="avatarInput" accept="image/png, image/jpeg, img/jpg">
                        <i aria-hidden="true" class="iconfont icon-upload fs-28"></i>
                        <p class="fs-15">上传新头像</p>
                    </label>
                </div>
            </form>
        </div>
        <div class="d-flex mt-3">
            <div class="me-auto">
            <?php if(session('user_id') && session('user_id') === $user->id): ?>
                <!-- <button type="button" class="btn btn-outline" data-bs-toggle="modal" data-bs-target="#editProfile">编辑个人资料</button> -->
                <a class="btn btn-light rounded-pill" href="/user/edit/<?= session('user_id') ?>">编辑个人资料</a>
            <?php else: ?>
                <a class="btn btn-secondary rounded-pill" href="#">关注</a>
            <?php endif ?>
            </div>
        </div>
    </div>
    <div class="py-3 px-4">
        <p class="fs-18 fw-bold text-truncate"><?= $user->username ?></p>
        <p class="mt-1 fs-15">发布有趣的科学、小玩意、历史、艺术等。订阅有深度的帖子。</p>
        <p class="mt-1 text-muted fs-14">
            <i class="iconfont icon-location"></i> 山东青岛
           <i class="iconfont icon-date ms-2"></i> <?= date("Y年m月", $user->created_at) ?> 加入
        </p>
        <!-- <p class="mt-1 fs-14">
            <a class="link-muted" href="#"><b>2423</b> 正在关注</a>
            <a class="link-muted" href="#"><b class="ms-3">84</b> 关注者</a>
        </p> -->
    </div>
</div>

