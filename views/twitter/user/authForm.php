<!doctype html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/font/iconfont.css">
    <link rel="stylesheet" href="/css/mini.css">
    <title>{block name="title"}{/block} | 迷你博客 | 心情日记 | 灵感记录 | 即时新闻</title>
</head>

<body>
    <!-- <div class="background">
        <div class="blur"></div>
    </div> -->
    <main class="app d-flex" role="main">
        {block name="main"}{/block}
    </main>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap5.bundle.min.js"></script>
    <script src="/js/editor.min.js"></script>
    <script src="/js/mini.js"></script>
    {block name="js"}{/block}
</body>

</html>