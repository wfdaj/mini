{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<!-- 顶部导航 -->
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon btn-sm me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

{include file="user/_user_cover_nav"}

<!-- 无帖子提示 -->
{if !$posts[0]}
    <div class="placeholder fs-28">无</div>
{/if}

<!-- 循环显示帖子列表 -->
<ul class="feed-list list-unstyled">
{foreach ($posts[0] as $post)}
    <li class="feed-item d-flex p-3 js-tap" data-href="/post/show/{$post->id}" data-pid="{$post->id}">
        <div class="feed-item-count">
            <spap class="post-num">
                <i>{$post->comments}</i>
                <span class="list-triangle-border"></span>
                <span class="list-triangle-body"></span>
            </spap>
        </div>
        <div class="d-flex flex-column w-100">
            <p class="typo-text">{$post->content}</p>
            {if $post->images > 0}
                {if $post->type === 'gif'}
                    <img class="img-fluid feed-item-img rounded align-self-baseline mt-2" data-gifffer="/upload/{$post->filename}" alt="" loading="lazy" decoding="async">
                {else /}
                    <img class="img-fluid feed-item-img rounded align-self-baseline mt-2" src="/upload/{$post->filename}" alt="" data-zoomable loading="lazy" decoding="async">
                {/if}
            {/if}
            <div class="d-flex align-items-center mt-1 fs-15">
                <a class="link-dark post-meta" href="/user/profile/{$post->user_id}">{$post->username}</a>
                <a class="link-secondary" href="/post/show/{$post->id}">{$post->created_at | nice_time}</a>
            </div>
        </div>
    </li>
{/foreach}
</ul>

<!-- 分页 -->
{php}echo pageLinks($posts);{/php}

{/block}

{block name="aside"}
<div class="search mt-2 mb-3">
    <i class="iconfont icon-search"></i>
    <input class="form-control search-input" type="text" placeholder="搜索">
</div>

<div class="mt-2 py-3 bg-light rounded-4">
    <h3 class="mb-3 px-3 fs-18">你可能会喜欢</h3>
    <div class="tile-item p-3 d-flex align-items-center">
        <img class="avatar" src="<?= getAvatar(1); ?>" alt="">
        <div>username</div>
    </div>
</div>

{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", () => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-post").classList.add('active');
    });
</script>
{/block}