{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

{include file="user/_user_cover_nav"}

<!-- 无帖子提示 -->
{if !$replies[0]}
    <div class="placeholder fs-28">无</div>
{/if}

<!-- 回帖列表 -->
<ul class="list-unstyled chat p-3">
{foreach $replies[0] as $reply}
    {if $reply->user_id !== session('user_id')}
    <li class="others mr-4">
        <div class="chat-item">
            <img class="avatar chat-avatar me-2" src="{$reply->user_id | getAvatar}" alt="">
            <div>
                <div class="chat-content">{$reply->content}</div>
                <span class="fs-14 text-muted">{$reply->username} · {$reply->post_created_at | nice_time}</span>
            </div>
        </div>
    </li>
    {else /}
    <li class="others mr-4">
        <div class="chat-item">
            <img class="avatar chat-avatar me-2" src="{$reply->post_user_id | getAvatar}" alt="">
            <div>
                <div class="chat-content">{$reply->title}</div>
                <span class="fs-14 text-muted">{$reply->post_user_id} · {$reply->post_created_at | nice_time}</span>
            </div>
        </div>
    </li>
    <li class="self mb-3">
        <div class="chat-item">
            <img class="avatar chat-avatar ms-2" src="{$reply->user_id | getAvatar}" alt="">
            <div>
                <div class="chat-content">{$reply->content}</div>
                <span class="fs-14 text-muted">{$reply->created_at | nice_time} · {$reply->username}</span>
            </div>
        </div>
    </li>
    {/if}
{/foreach}
</ul>

<!-- 分页 -->
{php}echo pageLinks($replies);{/php}

{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-replies").classList.add('active');
    });
</script>
{/block}