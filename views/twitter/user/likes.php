{extend name="layout/app" /}

{block name="title"}{$user->username}{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3">
    <div class="btn btn-icon me-3 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow"></i>
    </div>
    <h2 class="fs-18">{$user->username}</h2>
</div>

<!-- 用户信息 -->
{include file="user/_user_cover" /}

{include file="user/_user_cover_nav"}

<!-- 无帖子提示 -->
{if !$likes[0]}
    <div class="placeholder fs-28">无</div>
{/if}

<ul class="feed-list list-unstyled">
{foreach $likes[0] as $like}
    {if $like->content}
    <li class="feed-item d-flex p-3 js-tap" id="feed{$like->post_id}" data-href="/post/show/{$like->post_id}" data-pid="{$like->post_id}">
        <a href="/user/profile/{$like->user_id}" data-letters="{$like->username | mb_substr=0,2}"></a>
        <div class="d-flex flex-column w-100">
            <div class="d-flex align-items-center fs-15">
                <a class="link-dark fw-bold" href="/user/{$like->user_id}">{$like->username}</a>
                <a class="link-secondary ms-auto" href="/post/show/{$like->id}">{$like->created_at | nice_time}</a>
            </div>
            <p class="typo-text">{$like->content}</p>
            {if $like->images > 0}
                {php} $like->images = min(max(1, $like->images), 3); {/php}
                <div class="gallery mt-2">
                    <ul class="thumbnail-container feed-item-imgs list-unstyled row row-cols-{$like->images} mx-0">{php} echo getImagesList($like->images, $like->id); {/php}</ul>
                    <div class="fullscreen-container">
                        <div class="loading-indicator"><div class="spinner-border m-4" role="status"><span class="visually-hidden">Loading...</span></div></div>
                        <div class="image-wrapper">
                            <img class="fullscreen img-fluid rounded" src="" alt="">
                            <i class="iconfont icon-return prev-btn text-white" title="上一张"></i>
                            <i class="iconfont icon-enter next-btn text-white" title="下一张"></i>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    </li>
    {/if}
{/foreach}
</ul>


<!-- 分页 -->
{php}echo pageLinks($likes);{/php}

{/block}

{block name="js"}

<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        highlightActiveLink("user");
        document.getElementById("my-likes").classList.add('active');
    });
</script>
{/block}