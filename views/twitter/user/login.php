{extend name="user/authForm" /}

{block name="title"}登录{/block}

{block name="main"}
<div class="auth-form mx-auto mt-5">
    <h2 class="fs-24 my-4 text-center">😎 来了老弟～</h2>
    <form id="login-form" method="POST" autocomplete="off" action="login/submit">
        <input type="hidden" name="csrf_token" value="<?= csrf_token() ?>">
        <div class="form-floating mb-3">
            <input class="form-control" type="text" name="username" id="username" placeholder="用户名" autocomplete="off" required autofocus>
            <label for="username" id="usernameHelp">用户名</label>
        </div>
        <div class="form-floating mb-3">
            <input class="form-control" type="password" name="password" id="password" placeholder="密码" required>
            <label for="password" id="passwordHelp">密码</label>
        </div>
        <div class="form-floating mb-3 position-relative">
            <input class="form-control" type="text" name="captcha" id="captcha" placeholder="验证码" required>
            <img src="/auth/captcha" id="resetCaptcha" class="auth-captcha cursor-pointer" data-bs-toggle="tooltip" data-bs-title="点击刷新验证码">
            <label for="captcha" id="captchaHelp">验证码</label>
        </div>
        <button type="submit" class="btn btn-secondary w-100 p-3">
            登录
            <i class="icon iconfont icon-right-arrow btn--icon"></i>
        </button>
        <!-- <button type="button" id="submitButton" class="btn btn-secondary w-100 p-3">
            登录
            <i class="icon iconfont icon-right-arrow btn--icon"></i>
        </button> -->
    </form>
    <p class="text-center mt-3">
        <a href="/register" class="btn btn-link">没有账号？点此注册</a>
    </p>
    <hr class="hr-content" data-content="社交账号登录">
</div>
{/block}

{block name="js"}
<script>
    (function($) {
        $(document).ready(function() {
            const validations = {
                name: {
                    input: $('#username'),
                    help: $('#usernameHelp'),
                    minLength: 3
                },
                password: {
                    input: $('#password'),
                    help: $('#passwordHelp'),
                    minLength: 6
                },
                captcha: {
                    input: $('#captcha'),
                    help: $('#captchaHelp'),
                    minLength: 4
                }
            };
            const form = $('#login-form');
            const captchaImg = $("#resetCaptcha");
            const submitButton = $('#submitButton');
            let allInputsValid = false;
            let inputValidity = {}; // 添加一个对象来存储每个输入的验证状态

            // 验证单个输入字段
            const validateInput = type => {
                const {
                    input,
                    help,
                    minLength
                } = validations[type];
                const value = input.val().trim();
                const isValid = value.length >= minLength;
                const placeholderText = input.attr('placeholder');

                input.toggleClass('is-invalid', !isValid); // 根据验证结果切换类名
                help.toggleClass('text-danger', !isValid);
                help.text(!isValid ? `请输入至少 ${minLength} 位数` : placeholderText); // 设置或清空帮助文本并切换显示状态
                inputValidity[type] = isValid; // 更新验证状态到新的对象中

                return isValid;
            }

            // 验证所有输入字段
            const validateAllInputs = () => {
                allInputsValid = ['name', 'password', 'captcha'].every(validateInput); // 使用every进行所有输入的验证
                return allInputsValid;
            }

            // 绑定blur事件以验证输入
            const bindBlurEvents = () => {
                $.each(validations, (type) => {
                    validations[type].input.on('blur', () => validateInput(type));
                });
            };

            // 初始化blur事件绑定
            bindBlurEvents();

            submitButton.on('click', (event) => {
                event.preventDefault(); // 阻止默认提交行为
                if (!validateAllInputs()) return; // 如果输入无效，则不继续执行提交操作
                submitButton.prop('disabled', true); // 禁用提交按钮，防止重复提交

                $.ajax({
                    method: 'POST',
                    url: '/login/submit',
                    data: new FormData(form[0]),
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(res) {
                        submitButton.prop('disabled', false); // 重新启用提交按钮
                        if (res.status === 'success') {
                            toast.success(res.message); // 显示成功消息
                            setTimeout(() => document.referrer ? window.history.back() : window.location.href = "/", 2000); // 根据referrer重定向或返回上一页
                        } else {
                            toast.error(res.message); // 显示错误消息
                            captchaImg.attr("src", "/auth/captcha/" + Math.random()); // 更新验证码图片
                        }
                    },
                    error: function(xhr) {
                        submitButton.prop('disabled', false); // 重新启用提交按钮
                        toast.error('发生错误，请稍后再试。');
                        // console.error(xhr); // 在控制台中记录错误以供调试
                    }
                });
            });
        });
    })(jQuery);
</script>
{/block}