{extend name="install/layout" /}

{block name="title"}安装程序{/block}

{block name="main"}

<div class="d-flex flex-column p-3 mx-auto w-400">
    <h1 class="pt-4 pb-2 fs-20">安装</h1>
    <ul class="steps d-flex justify-content-between list-unstyled mb-5 text-muted">
        <li class="flex-fill">
            <p>1</p>
            <p>使用协议</p>
        </li>
        <li class="flex-fill done">
            <p>2</p>
            <p>环境检测</p>
        </li>
        <li class="flex-fill">
            <p>3</p>
            <p>写数据库</p>
        </li>
    </ul>
    <table class="mb-4">
        <thead>
            <tr>
                <th scope="col">环境</th>
                <th scope="col">需要</th>
                <th scope="col">当前</th>
                <th scope="col">结果</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>PHP版本</td>
                <td>^7.0</td>
                <td><?= PHP_VERSION ?></td>
                <td><?= $php_version ? '<span class="text-success">通过</span>' : '<span class="text-danger">未通过</span>' ?></td>
            </tr>
        </tbody>
    </table>

    <table class="mb-4">
        <thead>
            <tr>
                <th scope="col" colspan="2">目录权限检测</th>
            </tr>
        </thead>
        <tbody>
            {foreach $write as $k => $v}
                <?php if (!$v) $succeed = 0; ?>
                <tr>
                    <td><?= $k ?></td>
                    <td class="text-end"><?= $v ? '<span class="text-success">可写</span>' : '<span class="text-danger">不可写</span>' ?></td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="text-end">
        <a href="/install/" class="btn btn-light pe-6">上一步</a>
        {if $php_version && $succeed}
            <a href="/install/last" class="btn btn-secondary pe-6">下一步</a>
        {else /}
            <a href="javascript:;" class="btn btn-danger disabled pe-6" disabled>检测未通过</a>
        {/if}
    </div>
</div>

{/block}