{extend name="install/layout" /}

{block name="title"}安装程序{/block}

{block name="main"}

<div class=" flex-column p-3 mx-auto w-400">
    <h1 class="pt-4 pb-2 fs-20">安装</h1>
    <ul class="steps d-flex justify-content-between list-unstyled mb-5">
        <li class="flex-fill text-muted done">
            <p>1</p>
            <p>准备</p>
        </li>
        <li class="flex-fill text-muted done">
            <p>2</p>
            <p>环境检测</p>
        </li>
        <li class="flex-fill fw-bold done">
            <p>3</p>
            <p>写数据库</p>
        </li>
    </ul>
    <form action="/install/submit" method="post">
        <table class="mb-4">
            <thead>
                <tr>
                    <th scope="col" colspan="2">数据库信息</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>数据库地址</td>
                    <td><input type="text" class="form-control" name="host" id="host" value="localhost" required></td>
                </tr>
                <tr>
                    <td>数据库名</td>
                    <td><input type="text" class="form-control" name="database" id="database" required></td>
                </tr>
                <tr>
                    <td>用户名</td>
                    <td><input type="text" class="form-control" name="username" id="username" required></td>
                </tr>
                <tr>
                    <td>密码</td>
                    <td><input type="text" class="form-control" name="password" id="password" required></td>
                </tr>
                <tr>
                    <td>表前缀</td>
                    <td><input type="text" class="form-control" name="prefix" id="prefix"></td>
                </tr>
            </tbody>
        </table>
        <div class="text-end">
            <a href="/install/first" class="btn btn-light pe-6">上一步</a>
            <button type="submit" class="btn btn-secondary pe-6">提交</button>
        </div>
    </form>
</div>

{/block}