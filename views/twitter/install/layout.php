<!doctype html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/font/iconfont.css">
    <link rel="stylesheet" href="/css/mini.css">
    <title>{block name="title"}{/block} | 迷你博客 | 心情日记 | 灵感记录 | 即时新闻</title>
</head>

<body>
{block name="main"}{/block}
</body>

</html>