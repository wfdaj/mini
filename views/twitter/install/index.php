{extend name="install/layout" /}

{block name="title"}安装程序{/block}

{block name="main"}

<div class="d-flex flex-column p-3 mx-auto w-400">
    <h1 class="pt-4 pb-2 fs-20">安装</h1>
    <ul class="steps d-flex justify-content-between list-unstyled mb-4 text-muted">
        <li class="flex-fill done">
            <p>1</p>
            <p>使用协议</p>
        </li>
        <li class="flex-fill">
            <p>2</p>
            <p>环境检测</p>
        </li>
        <li class="flex-fill">
            <p>3</p>
            <p>写数据库</p>
        </li>
    </ul>
    <h2 class="fs-18 mb-2">开源软件使用协议</h2>
    <p class="mb-4">感谢您的选择。</p>
    <p class="mb-4"><b>免责声明</b></p>
    <ol class="list-ordered mb-4">
        <li>本软件的使用风险由用户自行承担。在使用本软件前，用户应自行评估并承担可能存在的风险和损失。</li>
        <li>开发者不对本软件的使用效果、功能和性能等方面做出任何保证或承诺。如果用户在使用本软件过程中遇到问题，可以向开发者寻求帮助和支持。</li>
    </ol>
    <p class="mb-4"><b>软件使用限制</b></p>
    <p class="mb-4">按照我国法律，在未取得相关资源（影片、动画、图书、音乐等）授权的情况下，请不要传播任何形式的相关资源（资源数据文件、种子文件、网盘文件、FTP 文件等）！</p>
    <div class="text-end">
        <a href="/install/first" class="btn btn-secondary p-3 pe-6">
            同意协议，继续安装
            <i class="btn--icon iconfont icon-right-arrow"></i>
        </a>
    </div>
</div>

{/block}