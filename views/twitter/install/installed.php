{extend name="install/layout" /}

{block name="title"}安装程序{/block}

{block name="main"}

<div class="app d-flex">
    <div class="auth-form my-4 mx-auto w-400">
        <p class="fs-32 text-success">✓</p>
        <h1 class="pb-4 fs-20">程序已安装完毕</h1>
        <p>若想重新安装，</p>
        <p>请删除 <code>config/install_lock.php</code> 文件。</p>

        <a href="/" class="btn btn-secondary mt-4" role="button">回到首页</a>
    </div>
</div>

{/block}