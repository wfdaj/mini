{extend name="layout/app" /}

{block name="title"}工具箱{/block}

{block name="main"}
<div class="breadcrumb d-flex justify-content-between align-items-center px-3 js-scroll js-autohide">
    {include file="layout/_avatar_offcanvas" /}
    <h2 class="fs-18 cursor-pointer js-top">工具箱</h2>
</div>
<div class="p-3 bg-light">
    <div class="container px-0">
        <div class="row mb-4 mt-1">
            <div class="col">
                <div class="card p-3 d-flex align-items-center">
                    <a class="stretched-link" href="/tool/symbols">特殊符号大全</a>
                    <i class="iconfont icon-smile ms-auto fs-28 text-primary opacity-50"></i>
                </div>
            </div>
            <div class="col">
                <div class="card p-3 d-flex align-items-center">
                    <p>万年历</p>
                    <i class="iconfont icon-date ms-auto fs-28 opacity-50"></i>
                </div>
            </div>
            <div class="col">
                <div class="card p-3 d-flex align-items-center">
                    <p>十二星座</p>
                    <i class="iconfont icon-birthday ms-auto fs-28 opacity-50"></i>
                </div>
            </div>
        </div>
    </div>
</div>
{/block}