{extend name="layout/app" /}

{block name="title"}探索{/block}

{block name="main"}
    <div class="breadcrumb d-flex justify-content-between align-items-center mb-1 px-3">
        <h2 class="fs-18 cursor-pointer js-top">探索</h2>
        <div class="search">
            <i class="iconfont icon-search"></i>
            <input class="form-control search-input py-2" type="text" placeholder="搜索">
        </div>
    </div>

    <!-- <nav class="nav nav-justified border-bottom">
        <a class="nav-link active" id="my-post" href="/">推荐</a>
        <a class="nav-link" id="my-replies" href="/">热门</a>
        <a class="nav-link" id="my-likes" href="/">用户</a>
        <a class="nav-link" id="my-fav" href="/">群</a>
    </nav> -->

    <div class="flash-news">
        <img src="img/flash-news.jpg" alt="">

        <div class="flash-mask">
            <h2 class="fs-20 py-1">中华人民共和国</h2>
            <p class="fs-13">13号说明字体</p>
        </div>
    </div>

{/block}

{block name="aside"}
    <div class="tile mt-2">
        <h2 class="px-3 pb-2 fs-18 fw-bold">有什么新鲜事？</h2>
        <div href="###" class="tile-item d-flex">
            <img src="/img/avatar.jpg" width="79" height="79" alt="">
            <div class="d-flex flex-column">
                <div class="fs-15 fw-bold">摔跤大赛是真打吗？</div>
                <div class="fs-13 text-muted">摔角 . 昨天</div>
            </div>
        </div>
        <div class="tile-item d-flex flex-column">
            <div class="fs-13 text-muted">California 的趋势</div>
            <div class="fs-15 fw-bold">Buying</div>
            <div class="fs-13 text-muted">16</div>
            <!-- <button class="btn btn-icon btn-sm tile-corner" role="button">
                <i class="iconfont icon-more fs-18"></i>
            </button> -->
        </div>
    </div>
{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", () => {
        // 当前链接添加 active
        highlightActiveLink("explore");
        // document.getElementById("my-post").classList.add('active');
    });
</script>
{/block}