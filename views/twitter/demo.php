{extend name="app" /}

{block name="title"}css{/block}

{block name="main"}
<ul>
    <li>中华人民共和国</li>
    <li>
        中华人民共和国
        <ul>
            <li>山东</li>
            <li>河南</li>
            <li>
                广东
                <ul>
                    <li>来了</li>
                    <li>来了</li>
                </ul>
            </li>

        </ul>
    </li>
</ul>

<mark>划重点</mark>

<hr>

<div class="text-highlight">需要注意的是，起始和结束颜色都是完全透明的，</div>

<hr>

<table>
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2" class="bg-danger text-danger">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>

<hr>

<div class="form-floating mb-3">
  <input type="email" id="floatingInput" placeholder="name@example.com">
  <label for="floatingInput">Email address</label>
</div>
<div class="form-floating mb-3">
  <input type="password" id="floatingPassword" placeholder="Password">
  <label for="floatingPassword">Password</label>
</div>

<input type="text" placeholder="默认输入框">

<hr>

<div>
  <button class="btn" type="button">主按钮</button>
  <button class="btn btn-secondary" type="button">二级按钮</button>
</div>

<div class="my-4">底部</div>
{/block}
