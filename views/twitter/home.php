{extend name="layout/app" /}

{block name="title"}主页{/block}

{block name="main"}
<div class="breadcrumb d-flex justify-content-between align-items-center px-3">
    {include file="layout/_avatar_offcanvas" /}
    <h2 class="fs-18 cursor-pointer js-top">主页</h2>
    <div class="dropdown">
        <button class="btn btn-sm btn-icon" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="iconfont icon-magic"></i></button>
        <div class="dropdown-menu animated-dropdown dropdown-menu-end shadow-sm">
            <div class="dropdown-item p-3 border-bottom text-center arrow bg-white">
                <img class="mb-2" src="/img/magic.svg" width="48" height="48" alt="">
                <p class="fw-bold fs-18">主页优先显示热门帖子</p>
            </div>
            <a class="dropdown-item" href="#">
                <i class="iconfont icon-exchange me-2"></i>
                <span class="fw-bold">切换查看最新帖子</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="iconfont icon-settings me-2"></i>
                <span class="fw-bold">查看内容偏好设置</span>
            </a>
        </div>
    </div>
</div>

<div class="alert mb-0 p-4 border-0 border-bottom text-center alert-dismissible fade show" id="js-show-once" role="alert">
    <b class="fs-20">每日帖不过３😉</b>
    <p class="mt-1 mb-3 text-muted">友情提示，发帖和回帖都需要<i class="squiggle">扣除１枚金币</i>哦！</p>
    <a class="btn btn-secondary rounded-pill" href="/reward/">
        <i class="iconfont icon-present me-2"></i>领取今日登录奖励
    </a>
</div>

{include file="layout/_post_list" /}

<?= pageLinks($posts); ?>

{/block}

{block name="aside"}
<div class="search mt-2 mb-3">
    <i class="iconfont icon-search"></i>
    <input class="form-control search-input" type="text" placeholder="搜索功能开发中...">
</div>

<div class="tile">
    <h2 class="px-3 pt-1 pb-2 fs-18 fw-bold">有什么新鲜事？</h2>
    <div href="###" class="tile-item d-flex">
        <img src="/img/avatar.jpg" width="79" height="79" alt="">
        <div class="d-flex flex-column">
            <div class="fs-15 fw-bold">摔跤大赛是真打吗？</div>
            <div class="fs-13 text-muted">摔角 . 昨天</div>
        </div>
    </div>
    <div class="tile-item d-flex flex-column">
        <div class="fs-13 text-muted">科幻</div>
        <div class="fs-15 fw-bold">测试</div>
        <div class="fs-13 text-muted"><i class="iconfont icon-analytics"></i> 114</div>
        <!-- <button class="btn btn-icon btn-sm tile-corner" role="button">
            <i class="iconfont icon-more fs-18"></i>
        </button> -->
    </div>
</div>
{/block}

{block name="js"}
<script src="/js/post.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        // 当前链接添加 active
        highlightActiveLink("home");
        // document.getElementById("my-post").classList.add('active');
        // 获取所有具有 .post-actions 类的元素
        const postActionsElements = document.querySelectorAll('.post-actions');
        postActionsElements.forEach((element) => {
            element.classList.add('home-post-actions');
        });
    });
</script>
{/block}