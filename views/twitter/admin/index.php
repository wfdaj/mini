{extend name="layout/app" /}

{block name="title"}后台{/block}

{block name="main"}
{include file="admin/_breadcrumb" /}

{include file="admin/_nav" /}

<div class="p-3 bg-light">
    <div class="container px-0">
        <div class="d-flex pt-3 pb-4 px-1">
            <div>
                <h1 class="fs-18 mb-1">你好，管理员</h1>
                <p class="fs-14 text-muted">
                    <i class="iconfont icon-time fs-14"></i>
                    上次登录 {$user->updated_at|nice_time}前
                </p>
            </div>
            <div class="ms-auto">
                <a class="btn btn-sm btn-secondary" id="clearCacheBtn" href="/admin/remove"><i class="iconfont icon-trash me-2"></i>清空缓存</a>
            </div>
        </div>
        <div class="row mb-4 mt-1">
            <div class="col">
                <div class="tile d-flex align-items-center p-3 bg-white rounded-3">
                    <div class="d-flex flex-column">
                        <span class="fs-14 text-muted">帖子</span>
                        <span class="fs-24">{$totalPosts}</span>
                    </div>
                    <i class="iconfont icon-feather ms-auto fs-28 text-primary opacity-50"></i>
                </div>
            </div>
            <div class="col">
                <div class="tile d-flex align-items-center p-3 bg-white rounded-3">
                    <div class="d-flex flex-column">
                        <span class="fs-14 text-muted">回帖</span>
                        <span class="fs-24">{$totalComments}</span>
                    </div>
                    <i class="iconfont icon-comment ms-auto fs-28 text-danger opacity-50"></i>
                </div>
            </div>
            <div class="col">
                <div class="tile d-flex align-items-center p-3 bg-white rounded-3">
                    <div class="d-flex flex-column">
                        <span class="fs-14 text-muted">会员</span>
                        <span class="fs-24">{$totalMembers}</span>
                    </div>
                    <i class="iconfont icon-pal ms-auto fs-28 text-success opacity-50"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="tile p-3 bg-white rounded-3 mb-4">
        <h3 class="mb-3">站点模式</h3>
        <div class="row row-cols-3 gy-3">
            <div class="col">
                <div class="form-check form-switch mb-1">
                    <input class="form-check-input js-switch" type="checkbox" role="switch" id="switchDebug" data-file="app" data-key="debug" <?= config('app.debug') ? 'checked' : ''; ?>>
                </div>
                <label class="form-check-label fs-15" for="switchDebug">调试模式</label>
            </div>
            <div class="col">
                <div class="form-check form-switch mb-1">
                    <input class="form-check-input js-switch" type="checkbox" role="switch" id="switchTrace" data-file="app" data-key="trace" <?= config('app.trace') ? 'checked' : ''; ?>>
                </div>
                <label class="form-check-label fs-15" for="switchTrace">追踪模式</label>
            </div>
            <div class="col">
                <div class="form-check form-switch mb-1">
                    <input class="form-check-input js-switch" type="checkbox" role="switch" id="switchMaintenance" data-file="app" data-key="maintenance" <?= config('app.maintenance') ? 'checked' : ''; ?>>
                </div>
                <label class="form-check-label fs-15" for="switchMaintenance">维护模式</label>
            </div>
        </div>
    </div>

    <div class="tile p-3 bg-white rounded-3 mb-4">
        <h3 class="mb-3">服务器信息</h3>
        <div class="row row-cols-3 gy-3">
            <div class="col">
                <p>{$Think.const.PHP_OS_FAMILY}</p>
                <span class="text-muted fs-14">操作系统</span>
            </div>
            <div class="col">
                <p>{$Think.const.PHP_VERSION}</p>
                <span class="text-muted fs-14">PHP</span>
            </div>
            <div class="col">
                <p>{$mysqlVersion}</p>
                <span class="text-muted fs-14">MySQL</span>
            </div>
            <div class="col">
                <p><?= ini_get('upload_max_filesize'); ?></p>
                <span class="text-muted fs-14">上传文件大小</span>
            </div>
            <div class="col">
                <p><?= ini_get('memory_limit'); ?></p>
                <span class="text-muted fs-14">内存上限</span>
            </div>
            <div class="col">
                <p><?= ini_get('max_execution_time'); ?></p>
                <span class="text-muted fs-14">最长运行时间</span>
            </div>
            <div class="col">
                <p><?= ini_get('safe_mode') ? '是' : '否'; ?></p>
                <span class="text-muted fs-14">安全模式</span>
            </div>
            <div class="col">
                <p><?= get_ip(); ?></p>
                <span class="text-muted fs-14">客户端 IP</span>
            </div>
            <div class="col">
                <p><?= $_SERVER['SERVER_ADDR'] ?? '未知'; ?></p>
                <span class="text-muted fs-14">服务端 IP</span>
            </div>
        </div>
    </div>

    <div class="tile p-3 bg-white rounded-3 mb-4">
        <h3 class="mb-3">开发人员</h3>
        <ul class="list-unordered lh-md">
            <li>官&nbsp;&nbsp;网：<a href="http://khpai.com" target="_blank">khpai.com</a></li>
            <li>Gitee：<a href="https://gitee.com/wfdaj/mini" target="_blank">gitee.com/wfdaj/mini</a></li>
            <li class="cursor-pointer d-inline" data-bs-toggle="collapse" href="#qqGroupQR" aria-expanded="false" aria-controls="qqGroupQR">
                <span>QQ群：182 885 562</span>
                <i class="iconfont icon-info" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="点击显示群二维码"></i>
            </li>
        </ul>
        <div class="collapse" id="qqGroupQR">
            <div class="box p-3">放置您的群二维码</div>
        </div>
    </div>
</div>
{/block}

{block name="aside"}
<div class="mt-2 p-3 bg-light rounded-4">
    <h3 class="section-title mb-2 fs-18">仪表盘</h3>
    <p>论坛各项数据一览</p>
</div>
{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        document.getElementById("admin-home").classList.add('active');
        // 切换配置项
        $('.js-switch').change(function() {
            const file = $(this).data("file");
            const key = $(this).data("key");
            const isChecked = $(this).is(':checked');
            // 准备要发送到服务器的数据
            const postData = {
                file: file,
                key: key,
                value: isChecked
            };
            $.ajax({
                url: '/admin/switch',
                type: 'POST',
                dataType: 'json',
                data: postData,
                success: function(res) {
                    console.log(res);
                    if (res.status === 'success') {
                        toast.success(res.message);
                        setTimeout(() => { window.location.reload(); }, 1000);
                    } else {
                        toast.error(res.message);
                    }
                },
                error: function(xhr, status, error) {
                    toast.error('有点小错误。');
                    console.error("AJAX Error: " + error);
                }
            });
        });
    });
</script>
{/block}