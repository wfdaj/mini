{extend name="layout/app" /}

{block name="title"}后台{/block}

{block name="main"}
{include file="admin/_breadcrumb" /}

{include file="admin/_nav" /}

<div class="bg-light">
    <div class="p-3">
        <h3>会员列表</h3>
        <p class="fs-14 text-muted mt-1">共有会员 {$totalMembers} 人</p>
    </div>
    <div class="d-flex justify-content-end">
        <!-- 点击展开的搜索框 -->
        <form class="search" id="searchform">
            <i class="iconfont icon-search"></i>
            <input class="form-control form-control-lg search-input rounded-0" id="searchInput" type="text" placeholder="搜索会员">
        </form>
        <!-- 搜索按钮 -->
        <button type="button" class="btn btn-icon rounded-0" id="toggleSearch"><i class="iconfont icon-search"></i></button>
        <!-- 过滤会员 -->
        <div class="dropdown">
            <button type="button" class="btn btn-icon rounded-0" data-bs-toggle="dropdown" aria-expanded="false" title="筛选">
                <i class="iconfont icon-filter"></i></button>
            <div class="dropdown-menu dropdown-menu-end shadow-sm">
                <a class="dropdown-item" href="#">版主</a>
                <a class="dropdown-item" href="#">一级会员</a>
                <a class="dropdown-item" href="#">禁止用户</a>
            </div>
        </div>
        <!-- 添加会员 -->
        <button type="button" class="btn btn-secondary btn-lg pe-6 fs-16" data-bs-toggle="modal" data-bs-target="#addMembersModal">添加会员</button>
    </div>
</div>

<!-- 会员列表 -->
<table class="table-hover mb-3 fs-15">
    <thead class="bg-lighter">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">用户名</th>
            <th scope="col">用户组</th>
            <th scope="col">注册时间</th>
            <th scope="col">邮箱</th>
            <th scope="col">编辑</th>
        </tr>
    </thead>
    <tbody>
        {foreach $users[0] as $user}
        <tr class="userList">
            <th scope="row">{$user->id}</th>
            <td><a href="/user/profile/{$user->id}" target="_blank">{$user->username}</a></td>
            <td>{$user->group_id | getUserGroupCategory}</td>
            <td title="{$user->created_at | date='Y-n-j G:i'}">{$user->created_at | nice_time}</td>
            <td class="mw-8 text-truncate userList-email" data-email-shown="false"><span class="userList-emailAddress">{$user->email}</span></td>
            <td>
                <a class="link-dark edit-user-link cursor-pointer" data-uid="{$user->id}" data-bs-toggle="modal" data-bs-target="#editUserModal">
                    <i class="iconfont icon-edit fs-18"></i>
                </a>
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>

<!-- 分页 -->
<?= pageLinks($users); ?>
{/block}

{block name="aside"}
<div class="mt-2 p-3 bg-light rounded-4">
    <h3 class="section-title mb-2 fs-18">用户组编号</h3>
    <ul class="list-unordered lh-md">
        <li>0 游客</li>
        <li>1 管理员</li>
        <li>2 版主</li>
        <li>3 禁言用户</li>
        <li>11 武林新丁</li>
        <li>12 江湖小虾</li>
        <li>13 后起之秀</li>
        <li>14 武林高手</li>
        <li>15 风尘奇侠</li>
        <li>16 无双隐士</li>
        <li>17 世外高人</li>
        <li>18 江湖侠隐</li>
    </ul>
</div>
{/block}

{block name="js"}
<!-- 添加会员弹出层 -->
<div class="modal fade" id="addMembersModal" tabindex="-1" aria-labelledby="addMembersModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content rounded-0">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="addMembersModalLabel">添加新会员</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="addUserForm" method="POST" autocomplete="off">
                    <div class="form-floating mb-3">
                        <input class="form-control" type="text" name="username" id="username" placeholder="用户名" autocomplete="off" required autofocus>
                        <label for="username" id="usernameHelp">用户名</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input class="form-control" type="email" name="email" id="email" placeholder="邮箱" autocomplete="off" required>
                        <label for="email" id="emailHelp">邮箱</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input class="form-control" type="password" name="password" id="password" placeholder="密码" required>
                        <label for="password" id="passwordHelp">密码</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="usergroup" id="guest" value="0">
                        <label class="form-check-label" for="guest">游客</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="usergroup" id="novice" value="11" checked>
                        <label class="form-check-label" for="novice">武林新丁</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="usergroup" id="moderator" value="2">
                        <label class="form-check-label" for="moderator">版主</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="usergroup" id="admin" value="1">
                        <label class="form-check-label" for="admin">管理员</label>
                    </div>
                </form>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-lg pe-6" data-bs-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary btn-lg pe-6" id="addUserBtn">添加用户</button>
            </div>
        </div>
    </div>
</div>

<!-- 编辑用户信息弹出层 -->
<div class="modal fade" id="editUserModal" tabindex="-1" aria-labelledby="editUserModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content rounded-0">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="editUserModalLabel">编辑用户信息</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="editUserForm" method="POST" autocomplete="off">
                    <div class="form-floating mb-3">
                        <input class="form-control" type="text" name="username" placeholder="用户名" autocomplete="off" required autofocus>
                        <label for="username" id="usernameHelp">用户名</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input class="form-control" type="email" name="email" placeholder="邮箱" autocomplete="off" required>
                        <label for="email" id="emailHelp">邮箱</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input class="form-control" type="password" name="password" placeholder="密码" required>
                        <div class="mt-1 text-muted fs-14"><i class="iconfont icon-info me-1"></i>不修改密码请留空</div>
                        <label for="password" id="passwordHelp">密码</label>
                    </div>
                </form>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-secondary btn-lg pe-6" data-bs-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary btn-lg pe-6" id="addUserBtn">编辑用户</button>
            </div>
        </div>
    </div>
</div>
<script src="/js/admin.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        document.getElementById("admin-users").classList.add('active');

        // 点击展开的搜索框
        // 缓存选择器结果
        const toggleSearch = document.getElementById('toggleSearch');
        const searchForm = document.getElementById('searchform');
        const searchInput = document.getElementById('searchInput');

        // 事件处理器函数
        function handleToggleSearchClick(e) {
            e.preventDefault();
            searchForm.classList.toggle('active');
            toggleSearch.classList.toggle('d-none');

            if (searchForm.classList.contains('active')) {
                searchInput.focus();
            } else {
                searchInput.blur();
            }

            e.stopPropagation();
        }

        function handleDocumentClick(e) {
            if (
                e.target !== toggleSearch &&
                !searchForm.contains(e.target) &&
                searchForm.classList.contains('active')
            ) {
                searchForm.classList.remove('active');
                toggleSearch.classList.remove('d-none');
            }
        }

        // 绑定事件监听器
        toggleSearch.addEventListener('click', handleToggleSearchClick);
        document.addEventListener('click', handleDocumentClick);

        // 显示或隐藏用户邮箱
        const elements = document.querySelectorAll('.userList-email');
        elements.forEach(function(e) {
            e.addEventListener('click', function() {
                var that = this;

                // 使用三元运算符直接切换 data-email-shown 的值
                var newShownValue = this.getAttribute('data-email-shown') === 'false' ? 'true' : 'false';
                this.setAttribute('data-email-shown', newShownValue);

                // 设置一个10秒后的定时器，将 data-email-shown 恢复为 false
                setTimeout(function() {
                    that.setAttribute('data-email-shown', 'false');
                }, 10000); // 10000毫秒 = 10秒
            });
        });

        // 弹出层，编辑用户数据
        $('.edit-user-link').on('click', function(e) {
            e.preventDefault(); // 阻止链接默认行为
            var uid = $(this).data('uid'); // 获取 data-uid 属性

            // 发送 AJAX 请求获取用户数据
            $.ajax({
                url: '/admin/member/' + uid, // 服务器脚本路径
                type: 'POST',
                dataType: 'json',
                success: function(user) {
                    // var userData = JSON.stringify(response);
                    // console.log(user);
                    // 使用获取到的用户数据填充模态框，例如：
                    $('#editUserModal input[name="username"]').val(user.username);
                    $('#editUserModal input[name="email"]').val(user.email);
                },
                error: function(xhr, status, error) {
                    console.error("AJAX Error: " + error);
                }
            });
        });
    });
</script>
{/block}