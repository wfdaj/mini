{extend name="layout/app" /}

{block name="title"}后台{/block}

{block name="main"}
{include file="admin/_breadcrumb" /}

{include file="admin/_nav" /}

<div class="container p-3 bg-light">
    <h3 class="pb-3">当前主题</h3>
    <div class="row gx-3">
        <div class="col-4">
            <img class="img-fluid rounded-3 mb-2 theme-active shadow-sm" src="/img/{$theme}.png" data-full="/img/{$theme}.png" alt="" loading="lazy" decoding="async" data-zoomable>
        </div>
        <div class="col-8 lh-md fs-15">
            <p>{$theme}</p>
            <button class="btn btn-secondary btn-sm mt-2" type="button">已启用</button>
        </div>
    </div>

    <h3 class="pt-4 pb-3">已安装主题</h3>
    <div class="row g-3">
        {foreach $folders as $folder}
        <div class="col-4">
            <img class="img-fluid rounded-3 mb-2" src="/img/{$folder}.png" data-full="/img/{$folder}.png" alt="" loading="lazy" decoding="async" data-zoomable>
        </div>
        <div class="col-8 lh-md fs-15">
            <p>{$folder}</p>
            <button class="btn btn-outline btn-sm mt-2 js-enable" type="button" data-file="app" data-key="theme" data-value="{$folder}">启用</button>
        </div>
        {/foreach}
    </div>
</div>
{/block}

{block name="aside"}
<div class="mt-2 p-3 bg-light rounded-4">
    <h3 class="section-title mb-2 fs-18">设置</h3>
    <p>设置论坛名称、语言以及其他基础设置</p>
</div>
{/block}

{block name="js"}
<script src="/js/medium-zoom.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 图片放大
        mediumZoom('[data-zoomable]', {
            margin: 30,
            background: 'rgba(0,0,0,.7)',
        });
        // 当前链接添加 active
        document.getElementById("admin-theme").classList.add('active');

        // 切换配置项
        $('.js-enable').click(function() {
            const file = $(this).data("file");
            const key = $(this).data("key");
            const value = $(this).data("value");
            // 准备要发送到服务器的数据
            const postData = {
                file: file,
                key: key,
                value: value
            };
            $.ajax({
                url: '/admin/conf',
                type: 'POST',
                dataType: 'json',
                data: postData,
                success: function(res) {
                    console.log(res);
                    if (res.status === 'success') {
                        toast.success(res.message);
                    } else {
                        toast.error(res.message);
                    }
                },
                error: function(xhr, status, error) {
                    toast.error('有点小错误。');
                    console.error("AJAX Error: " + error);
                }
            });
        });
    });
</script>
{/block}