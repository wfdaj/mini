{extend name="layout/app" /}

{block name="title"}后台{/block}

{block name="main"}
{include file="admin/_breadcrumb" /}

{include file="admin/_nav" /}

<div class="p-3 bg-light">
    <form action="" class="p-3">
        <div class="mb-3">
            <label for="siteTitle" class="mb-2">站点名称</label>
            <input type="text" class="form-control" id="siteTitle" name="siteTitle" value="<?= config('app.name'); ?>">
        </div>
        <div class="mb-3">
            <label for="siteDescription" class="mb-2">站点描述</label>
            <input type="text" class="form-control" id="siteDescription">
            <div class="mt-1 text-muted typo-text fs-14">输入一两句话简单介绍您的网站。一般为 160 字左右的文本，用于介绍站点的内容。平常多被搜索引擎截取网页简介用。</div>
        </div>
        <div class="mb-3">
            <label for="welcomeMessage" class="mb-2">欢迎横幅</label>
            <input type="text" class="form-control" id="welcomeMessage">
            <div class="mt-1 text-muted typo-text fs-14">首页横幅文本，可以用于欢迎论坛访客。</div>
        </div>
        <button type="button" class="btn btn-secondary mb-3 pe-6">保存</button>
    </form>
</div>
{/block}

{block name="aside"}
<div class="mt-2 p-3 bg-light rounded-4">
    <h3 class="section-title mb-2 fs-18">设置</h3>
    <p>设置论坛名称、语言以及其他基础设置</p>
</div>
{/block}

{block name="js"}
<script>
    document.addEventListener("DOMContentLoaded", (e) => {
        // 当前链接添加 active
        document.getElementById("admin-setting").classList.add('active');
    });
</script>
{/block}