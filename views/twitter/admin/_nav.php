<nav class="nav nav-justified border-bottom">
    <a class="nav-link" href="/admin" id="admin-home">仪表盘</a>
    <a class="nav-link" href="/admin/setting" id="admin-setting">设置</a>
    <a class="nav-link cursor-not-allowed" href="/admin/permissions" id="admin-permissions">用户组</a>
    <a class="nav-link" href="/admin/theme" id="admin-theme">主题</a>
    <a class="nav-link" href="/admin/users" id="admin-users">会员</a>
</nav>