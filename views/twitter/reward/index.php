{extend name="layout/app" /}

{block name="title"}领金币{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center px-3 js-scroll">
    <div class="btn btn-icon btn-sm me-4 js-back" aria-label="返回" role="button" tabindex="0">
        <i class="iconfont icon-left-arrow fs-20"></i>
    </div>
    <h2 class="w-100 fs-18 hand js-top"><span class="text-truncate">领金币</span></h2>
</div>

<div class="d-flex justify-content-around p-4 text-muted fs-15 bg-light">
    <div>
        <p class="fs-24 text-success"><?= isset($signed) ? '3' : '0'; ?></p>
        <span>今日已领</span>
    </div>
    <div>
        <p class="fs-24 text-dark"><?= isset($last_sign_time) ? $last_sign_time.'<span class="time-text">小时前</span>' : '-' ;?></p>
        <span>上次签到时间</span>
    </div>
    <div>
        <p class="fs-24 text-danger"><?= $user->golds ?? '-' ;?></p>
        <span>可用金币</span>
    </div>
</div>

<div class="wave-bar mb-2">
    <svg viewBox="0 0 1152 73"><path d="M99.0331 0.252716C59.2655 0.284556 0 25.2197 0 25.2197V0.252716H99.0331C99.0585 0.252696 99.0839 0.252686 99.1093 0.252686C99.1538 0.252686 99.1982 0.252696 99.2427 0.252716H1152V73C1018.73 21.6667 957.818 24.4226 819.692 22.7693C672.54 21.008 573.085 73 427.919 73C308.414 73 218.068 0.307089 99.2427 0.252716H99.0331Z"></path></svg>
</div>

<nav class="nav nav-justified border-bottom">
  <a class="nav-link fs-18" id="my-reward" href="/reward">每日领取</a>
  <a class="nav-link fs-18 cursor-not-allowed" id="my-redeem" href="#">兑换礼物</a>
  <a class="nav-link fs-18 cursor-not-allowed" id="my-levels" href="#">等级</a>
</nav>

<div class="d-flex justify-content-around p-4 text-center">
    <div class="card p-4 border rounded me-4 text-dark text-decoration-none">
        <div class="placement fs-24 fw-bold">
            <span class="point <?= isset($signed) ? 'bg-success' : ''; ?>"></span> 3
        </div>
        <p><i class="iconfont icon-present fs-32 text-success"></i></p>
        <h3 class="fs-18 fw-bold">登录奖励</h3>
        <p class="fs-15 my-3">领取每日登录奖励，可用于发帖、回帖和下载附件。</p>
        <?php if(isset($signed)): ?>
            <button type="button" class="btn btn-outline" disabled>已领取</button>
        <?php else: ?>
            <button type="submit" class="btn btn-secondary" id="<?= isset($signed) ? '' : 'js-sign'; ?>">点击领取</button>
        <?php endif ?>
    </div>
    <div class="card p-4 border rounded">
        <p><i class="iconfont icon-redpacket fs-32 text-danger"></i></p>
        <h3 class="fs-18 fw-bold">红包</h3>
        <p class="fs-15 mt-3">模块正在制作，敬请期待。</p>
    </div>
</div>

{/block}

{block name="aside"}

<div class="mt-2 p-3 bg-light rounded-4">
    <h3 class="section-title mb-2 fs-18">登录奖励说明</h3>
    <p>每24小时可领取一次，每次 3 枚金币。</p>
    <p>发帖、回帖每次各需消耗 1 枚金币。</p>
</div>

{/block}


{block name="js"}
<script>
document.addEventListener("DOMContentLoaded", (event) => {
    // 高亮当前链接
    highlightActiveLink('reward');
    document.getElementById('my-reward').classList.add('active');
    // 签到
    const signElement = document.getElementById("js-sign");
    if (signElement) {
        event.preventDefault(); // 阻止默认行为，比如链接跳转
        signElement.addEventListener('click', function() {
            fetch('/reward/sign/', {method: 'POST'})
            .then(response => response.json())
            .then(res => {
                if (res.status === 'success') {
                    toast.success(res.message);
                    document.querySelector('.point').classList.add('bg-success');
                    // 禁用按钮并显示"已领取"文字
                    signElement.textContent = '已领取';
                    signElement.disabled = true;
                    // 1.5秒后刷新页面
                    setTimeout(() => location.reload(), 1500);
                } else {
                    toast.error(res.message);
                }
            })
            .catch(error => console.error(error)); // 处理错误，避免被忽略。
        });
    }
});
</script>
{/block}