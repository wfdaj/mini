<header id="app-header" class="d-flex justify-content-between align-items-center">
    <a class="btn btn-icon selected" href="/">
        <i class="icon iconfont icon-home fs-24"></i>
    </a>
    <a class="btn btn-icon" href="/">
        <i class="icon iconfont icon-search fs-24"></i>
    </a>
    <a class="btn btn-icon" href="/">
        <i class="icon iconfont icon-edit fs-24"></i>
    </a>
    <a class="btn btn-icon" href="/">
        <i class="icon iconfont icon-favorite fs-24"></i>
    </a>
    <a class="btn btn-icon" href="/login">
        <i class="icon iconfont icon-user fs-24"></i>
    </a>
</header>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var lastScrollTop = 0;
        var header = document.getElementById('app-header'); // 缓存选择器结果  
        var originalHeaderHeight = header.offsetHeight; // 存储原始高度  

        // 定义更新头部类的函数  
        function updateHeaderClass() {
            var st = window.pageYOffset || document.documentElement.scrollTop;

            if (st > lastScrollTop) {
                // 向下滚动  
                if (!header.classList.contains('scrolled')) { // 使用缓存的选择器结果  
                    header.classList.add('scrolled');
                }
            } else if (st === 0) {
                // 滚动到顶部  
                if (header.classList.contains('scrolled')) {
                    header.classList.remove('scrolled');
                }
            }

            lastScrollTop = st;
        }

        // 使用节流函数来限制滚动事件处理函数的执行频率  
        function throttle(func, delay) {
            var timeout;
            return function() {
                var context = this;
                var args = arguments;
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    func.apply(context, args);
                }, delay);
            };
        }

        // 绑定经过节流的滚动事件处理函数  
        window.addEventListener('scroll', throttle(updateHeaderClass, 100)); // 使用节流版本的事件处理器  
    });
</script>