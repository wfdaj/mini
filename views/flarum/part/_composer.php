<div class="composer d-flex flex-column mb-3 shadow-sm">
    <form id="post-form" method="POST" enctype="multipart/form-data" action="/post/submit">
        <textarea class="border-0 js-textarea-item" name="content" id="content" maxlength="200" spellcheck="false" placeholder="有什么新鲜事儿？" required autofocus></textarea>
        <input type="hidden" name="csrf_token" value="<?= csrf_token() ?>">
        <input type="file" class="d-none" name="files[]" id="files" accept="image/png,image/jpeg,image/gif">
        <div class="composer-action d-flex justify-content-between">
            {include file="part/_post_toolbar" /}
            <div class="d-flex align-items-center">
                <span class="textarea-count" id="counter">200</span>
                <!-- <button type="button" class="btn btn-primary fw-bold" disabled>金币不足</button> -->
                <!-- <button type="button" id="submitButton" class="btn btn-primary fw-bold">发帖</button> -->
                <button type="submit" class="btn btn-primary px-3 lh-1">发送</button>
            </div>
        </div>
    </form>
    <div>
        <img class="previewImg img-fluid rounded mb-2" id="previewImg">
    </div>
</div>

<script>
 // 发帖
 document.addEventListener('DOMContentLoaded', function() {
        const MAX_FILES = 4; // 最多允许上传的文件数量
        const fileInput = document.getElementById('files');
        let files = fileInput.files;

        // 验证发帖内容
        function validateContent() {
            const content = document.getElementById('content').value.trim();
            return content.length > 0;
        }

        // 文件数量验证
        function validateFileCount() {
            if (files.length > MAX_FILES) {
                toast.error(`一次最多上传 ${MAX_FILES} 张图片！`);
                return false;
            }
            return true;
        }

        // 文件输入更改时更新文件列表并执行验证
        fileInput.addEventListener('change', function() {
            files = fileInput.files;
            validateFileCount();
        });

        // 点击发帖按钮后验证
        const submitButton = document.querySelector('.submitButton');
        if (submitButton) {
            submitButton.addEventListener('click', function() {
                if (!validateFileCount()) return;
                if (!validateContent()) {
                    toast.info('请输入帖子内容');
                    return;
                }
                let data = new FormData(document.getElementById('post-form'));
                fetch('/post/submit', {
                        method: 'POST',
                        body: data
                    })
                    .then(response => response.json())
                    .then(res => {
                        if (res.status === 'success') {
                            // 显示成功消息
                            toast.success(res.message);
                            setTimeout(() => {
                                location.reload();
                            }, 1500);
                        } else {
                            // 显示错误消息
                            toast.error(res.message);
                        }
                    })
                    .catch(error => {
                        toast.error('上传文件失败。');
                    });
            });
        }

        const fileEle = document.getElementById('files');
        const previewContainer = document.getElementById('previewContainer');
        let selectedFiles = [];

        // 创建一个用于预览的图片元素
        function createPreviewImage(file) {
            const img = document.createElement('img');
            img.classList.add('preview-image'); // 使用CSS类代替内联样式
            img.src = URL.createObjectURL(file);
            img.title = "点击删除图片";
            img.addEventListener('click', function() {
                removePreviewImage(this);
            });
            return img;
        }

        // 从预览容器中移除图片，并从文件列表中删除文件
        function removePreviewImage(img) {
            previewContainer.removeChild(img);
            const file = selectedFiles.find(f => URL.createObjectURL(f) === img.src);
            if (file) {
                const index = selectedFiles.indexOf(file);
                selectedFiles.splice(index, 1);
                URL.revokeObjectURL(img.src);
            }
        }

        fileEle.addEventListener('change', function(e) {
            previewContainer.innerHTML = '';
            selectedFiles = [];
            const files = e.target.files;
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                // 添加一个简单的文件类型检查
                if (!file.type.startsWith('image/')) {
                    toast.error('只支持上传图片 -_-"');
                    continue;
                }
                selectedFiles.push(file);
                const img = createPreviewImage(file);
                previewContainer.appendChild(img);
            }
        });

        /**
         * 计算剩余字符数
         */
        const contentEle = document.getElementById('content');
        const counterEle = document.getElementById('counter');
        contentEle.addEventListener('input', function(e) {
            const target = e.target;
            const maxLength = 300;
            // 计算当前的字符数
            const currentLength = getStringLength(target.value);
            counterEle.innerHTML = Math.floor(maxLength - currentLength);
        });
    });
</script>