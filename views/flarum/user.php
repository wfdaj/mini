{extend name="app" /}

{block name="title"}首页{/block}

{block name="main"}
<?php if($users[1]->totalRows !== 0): ?>
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center p-3">
        <li class="page-item"><a class="page-link" href="<?= $users[1]->prevPage;?>"><span aria-hidden="true">&lsaquo;</span></a></li>
        <?php foreach($users[1]->listPage as $k=>$v): ?>
            <?php if($k == $users[1]->currentPage): ?>
                <li class="page-item"><a class="page-link active" href="<?= $v ?>"><?= $k ?></a></li>
            <?php else: ?>
                <li class="page-item"><a class="page-link" href="<?= $v ?>"><?= $k ?></a></li>
            <?php endif ?>
        <?php endforeach ?>
        <li class="page-item"><a class="page-link" href="<?= $users[1]->nextPage;?>"><span aria-hidden="true">&rsaquo;</span></a></li>
    </ul>
</nav>
<?php endif ?>
{/block}