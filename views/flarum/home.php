{extend name="app" /}

{block name="title"}首页{/block}

{block name="header"}
{include file="part/_header" /}
{/block}

{block name="main"}
{include file="part/_composer" /}

<!-- <nav class="nav border-bottom">
    <a class="nav-link active" href="/" data-active="home-news">热门</a>
    <a class="nav-link" href="/user/follow" data-active="home-follow">正在关注</a>
</nav> -->

{include file="part/_post_list" /}

<?= pageLinks($posts); ?>

{/block}

{block name="js"}
<script src="/js/editor.min.js"></script>
<script>
    $(document).ready(function() {
        const textarea = $('.js-textarea-item');
        const composer = $('.composer');
        let isExpanded = false; // 引入状态变量来跟踪是否已经展开

        // 当textarea被点击时，处理展开逻辑
        textarea.on('click', function(e) {
            if (!isExpanded) {
                composer.addClass('expanded'); // 如果尚未展开，则添加expanded类来展开
                composer.css('overflow', 'visible');
                isExpanded = true; // 更新状态为已展开
            }
            // 不再执行收缩操作，因此移除了相关的else分支
            e.stopPropagation(); // 阻止事件冒泡到document
        });

        // 当textarea内容发生变化时，确保composer是展开的
        textarea.on('input', function() {
            if (!isExpanded) {
                composer.addClass('expanded'); // 如果尚未展开，则展开
                composer.css('overflow', 'visible');
                isExpanded = true; // 更新状态为已展开
            }
            // 如果已经展开，则不执行任何操作
        });
    });
</script>

{/block}