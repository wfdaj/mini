{extend name="app" /}

{block name="title"}帖子{/block}

{block name="main"}
<div class="breadcrumb d-flex align-items-center">
    <div class="btn btn-sm btn-icon me-3 js-back" aria-label="返回">
        <i class="iconfont icon-arrow-left"></i>
    </div>
    <h2 class="w-100 fs-18 hand js-top"><span class="text-truncate"><?= $page_title ?? '' ?></span></h2>
    <div class="dropdown ms-auto">
        <button class="btn btn-sm btn-icon" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="iconfont icon-overflow-menu-horizontal"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-end shadow-sm">
            <?php if($user) { ?>
                <?php if($post->user_id === $user->id){  ?>
                <a href="javascript:;" class="dropdown-item text-danger" id="js-del" data-post-id="<?= segment(3) ?>">
                    <i class="iconfont icon-trash me-2"></i>
                    <span class="fw-bold">删除</span>
                </a>
                <?php } ?>
            <?php } ?>
            <a class="dropdown-item" href="#">
                <i class="iconfont icon-shield me-2"></i>
                <span class="fw-bold">屏蔽</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="iconfont icon-report me-2"></i>
                <span class="fw-bold">举报 帖子</span>
            </a>
        </div>
    </div>
</div>

<div class="article p-3 border-bottom">
    <div class="d-flex align-items-center">
        <a href="/user/profile/<?= $post->user_id ?>" data-letters="<?= mb_substr($post->username, 0, 1, 'UTF-8'); ?>"></a>
        <p><b><?= $post->username ?></b></p>
        <div class="ms-auto text-muted">
            <time datetime="<?= date("Y-m-d H:i:s", $post->created_at) ?>"><?= nice_time($post->created_at) ?></time>
        </div>
    </div>
    <div class="typo-text py-3 fs-18" id="postContent"><?= $post->content ?></div>
    <?php if($post->images > 0): ?>
        <?php $post->images = min(max(1, $post->images), 3); ?>
        <div class="gallery mt-2">
            <ul class="thumbnail-container list-unstyled mx-0"><?= getImagesList($post->images, $post->id, false) ?></ul>
            </div>
        </div>
    <?php endif ?>
    <!-- 头像挂件 -->
    <!-- <div class="avatar avatar-pendant">
        <img src="/img/avatar_default.jpeg" data="face" alt="">
        <img src="/img/face/garb/fighters.png" data="pendant" alt="">
    </div> -->
    <div class="d-flex align-items-center py-3 fs-14 text-muted border-bottom">
        阅读用时: <span class="d-inline-block me-3" id="readingTime"></span>
        来自：<span>ip</span>
    </div>


    <div class="my-2 d-flex justify-content-around post-actions">
        <!-- 回复 -->
        <a role="button" class="post-action-reply cursor-arrow" data-bs-toggle="tooltip" data-bs-trigger="hover" title="评论数">
            <div class="d-flex justify-content-center post-action-icon">
                <i class="iconfont icon-chat"></i>
            </div>
            <span><?= $post->comments !== 0 ? $post->comments : '' ?></span>
        </a>


        <!-- 分享 -->
        <a href="javascript:;" class="post-action-fav" data-bs-toggle="tooltip" data-bs-trigger="hover" title="分享">
            <div class="d-flex justify-content-center post-action-icon">
                <i class="iconfont icon-upload"></i>
            </div>
        </a>
    </div>
</div>

<svg xmlns="http://www.w3.org/2000/svg" class="absolute -top-[9px] left-0 right-0 row-start-2 ml-[calc(50%-50vw)] h-px w-screen" fill="none"><defs><pattern id=":S1:" patternUnits="userSpaceOnUse" width="16" height="1"><line class="stroke-zinc-950 dark:stroke-white" x1="0" x2="16" y1="0.5" y2="0.5" stroke-dasharray="2 2" stroke-width="1.5" stroke-opacity="0.1" stroke-linejoin="round"></line></pattern></defs><rect width="100%" height="100%" fill="url(#:S1:)"></rect></svg>


{/block}

{block name="js"}
<script src="/js/medium-zoom.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
    mediumZoom('[data-zoomable]', {
        margin: 30,
        background: 'rgba(0,0,0,.7)',
    });

    const jDel = document.getElementById('js-del');

    if (jDel) {
        jDel.addEventListener('click', function () {
            let postId = this.getAttribute('data-post-id');
            fetch(`/post/del/${postId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ 'post_id': postId })
            })
            .then(response => response.json())
            .then(res => {
                if (res.status === 'success') {
                    toast.success(res.message);
                    setTimeout(function() {  
                        window.location.href = "/";  
                    }, 2000);
                } else {
                    toast.error(res.message);
                }
            })
            .catch(error => console.error('Error:', error));
        });
    }
});
</script>
{/block}