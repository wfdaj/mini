<!doctype html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="//at.alicdn.com/t/c/font_4489059_mlxbg35jk3g.css">
    <link rel="stylesheet" href="/css/mini.css">
    <title>{block name="title"}{/block} | 迷你博客 | 心情日记 | 灵感记录 | 即时新闻</title>
</head>

<body>
    <div class="container">
        {block name="header"}{/block}
        <main id="app-main" class="d-flex flex-column mx-auto" role="main">
            {block name="main"}{/block}
        </main>
    </div>

    <a href="/auth/logout" class="">退出</a>

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap5.bundle.min.js"></script>
    <script src="/js/editor.min.js"></script>
    <script src="/js/mini.js"></script>
    {block name="js"}{/block}
</body>

</html>