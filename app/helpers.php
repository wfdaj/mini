<?php

/**
 * 自定义助手函数
 */

/**
 * 检查用户登录状态，未登录则重定向到登录页面并显示一条消息
 *
 * @param string $message 重定向时显示的消息，默认为'请先登录'
 * @param string $status 消息状态，默认为'error'
 * @return void
 */
function loginCheck(string $message = '请先登录', string $status = 'error'): void
{
    $user_id = session('user_id');

    if ($user_id === null || $user_id === false) { // 明确检查null或false
        if ($message) { // 只在有消息时才调用flash
            flash($status, $message);
        }
        redirect(url('/login')); // 假设redirect函数内部已经处理了终止执行
    }
}

/**
 * 检查是否作者本人
 *
 * @param int $postAuthorId  作者id
 * @param int $currentUserId 当前用户id
 */
function isAuthor(int $postAuthorId, ?int $currentUserId)
{
    // 用户未登录，直接返回
    if ($currentUserId === null) {
        return false;
    }

    return $postAuthorId === $currentUserId;
}

/**
 * 是否管理员
 *
 * @return bool
 */
function isAdmin()
{
    $userId = (int) session('user_id');
    $user = model('user')::find($userId);

    if ($user && $user->group_id === 1 && $userId === 1) {
        return true;
    }

    return false;
}

/**
 * 获取用户头像
 *
 * @param int|null $user_id 用户ID，可能为null
 * @return string 头像URL
 */
function getAvatar(?int $user_id): string
{
    // 定义默认头像路径
    $defaultAvatar = '/img/avatar/default.jpg';
    // 定义头像目录和扩展名
    $avatarDir = '/img/avatar/';
    $avatarExtension = '.png';
    // 头像总数
    $avatarCount = 20;

    // 检查$user_id是否为null，如果是则直接返回默认头像
    if (is_null($user_id) || $user_id <= 0) {
        return $defaultAvatar;
    }

    $avatars = range(1, 20);
    // 使用crc32哈希函数并取绝对值，然后取模得到头像索引
    $avatarIndex = abs(crc32((string)$user_id)) % $avatarCount;
    $userAvatar  = "{$avatarIndex}{$avatarExtension}";

    return $avatarDir . $userAvatar;
}

/**
 * 返回缩略图路径，若无缩略图则返回原图路径
 *
 * @param  string $target_image 目标图片路径
 * @return string 返回缩略图或原图的路径
 */
function thumb(string $target_image): string
{
    $imgInfo = pathinfo($target_image);
    $thumbnailPath = "{$imgInfo['dirname']}/{$imgInfo['filename']}_thumb.{$imgInfo['extension']}";

    return is_file($thumbnailPath) ? $thumbnailPath : $target_image;
}

/**
 * 获取图片列表的HTML字符串
 *
 * @param int $imagesTotal 需要显示的图片总数
 * @param int $postId      帖子ID
 * @return string|bool 图片列表的HTML或false（如果没有图片或无效参数）
 */
function getImagesList(int $imagesTotal, int $postId, $thumb = true)
{
    // 调用模型方法获取图片列表
    $images = model('post')->getImagesList($postId);

    // 验证图片列表和图片总数
    if (empty($images) || $imagesTotal <= 0) {
        return false;
    }

    // 确保 $imagesTotal 不会超出图片列表的长度
    $imagesTotal = min($imagesTotal, count($images));

    // 初始化HTML字符串
    $html = '';

    // 循环遍历图片数组构建HTML
    for ($i = 0; $i < $imagesTotal; $i++) {
        $image = $images[$i];
        $thumbnail = thumb($image->filename);
        $fullImage = $image->filename;

        // 构建单张图片的HTML
        $singleImageHtml = sprintf(
            '<li class="col gx-1"><img class="feed-item-img js-no-click" src="/%s" data-full="%s" alt="" loading="lazy" decoding="async"></li>',
            escape($thumbnail),
            escape($fullImage)
        );

        $largeImageHtml = sprintf(
            '<li><img class="feed-item-img img-fluid rounded mb-3 js-no-click" src="/%s" alt="" loading="lazy" decoding="async" data-zoomable></li>',
            escape($fullImage)
        );

        if ($thumb) {
            // 追加到HTML字符串
            $html .= $singleImageHtml;
        } else {
            $html .= $largeImageHtml;
        }
    }

    // 返回构建好的HTML字符串
    return $html;
}

/**
 * 生成分页链接
 */
function pageLinks(array $data)
{
    if ($data[1]->totalRows === 0) {
        // return '<div class="placeholder fs-28">无</div>';
        return false;
    }

    $html = '<nav aria-label="Page navigation">';
    $html .= '<ul class="pagination justify-content-center p-3">';

    // 上一页链接
    if ($data[1]->currentPage === 1) {
        $html .= '';
    } else {
        $html .= '<li class="page-item">';
        $html .= '<a class="page-link" href="' . escape($data[1]->prevPage) . '">';
        $html .= '<i class="icon iconfont icon-return" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="上一页"></i>';
        $html .= '</a>';
        $html .= '</li>';
    }

    // 遍历页码列表
    foreach ($data[1]->listPage as $k => $v) {
        $html .= '<li class="page-item">';
        $html .= '<a class="page-link' . ($k == $data[1]->currentPage ? ' active' : '') . '" href="' . escape($v) . '">';
        $html .= escape($k);
        $html .= '</a>';
        $html .= '</li>';
    }

    // 下一页链接
    if ($data[1]->currentPage === $data[1]->maxPage) {
        $html .= '';
    } else {
        $html .= '<li class="page-item">';
        $html .= '<a class="page-link" href="' . escape($data[1]->nextPage) . '">';
        $html .= '<i class="icon iconfont icon-enter" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="下一页"></i>';
        $html .= '</a>';
        $html .= '</li>';
    }

    $html .= '</ul>';
    $html .= '</nav>';

    return $html;
}

/**
 * 根据传入的数字显示对应用户组
 */
function getUserGroupCategory($groupId)
{
    // 用户组类别
    $userGroups = [
        0  => '游客',
        1  => '管理员',
        2  => '版主',
        3  => '禁言用户',
        11 => '武林新丁',
        12 => '江湖小虾',
        13 => '后起之秀',
        14 => '武林高手',
        15 => '风尘奇侠',
        16 => '无双隐士',
        17 => '世外高人',
        18 => '江湖侠隐',
    ];

    // 直接返回用户组类别或使用默认值”未知用户组“
    return $userGroups[$groupId] ?? "未知用户组";
}


/**
 * 登录或注册后跳转到之前页面
 */
function goBack()
{
    if (session('backToLink')) {
        return redirect(session('backToLink'));
    }

    return redirect(url('/'));
}

/**
 * 输出当前日期及对应的中文星期
 *
 * @param string $chinese 星期前缀，默认为'周'
 * @return string 格式化后的日期及星期字符串
 */
function todayWeekday(string $chinese = '周'): string
{
    $weekdays = ['日', '一', '二', '三', '四', '五', '六'];
    $weekdayIndex = date('N') - 1; // 'N' 返回 1(周一) 到 7(周日)
    $weekday = $weekdays[$weekdayIndex]; // 获取中文星期字符

    return date("Y-m-d H:i") . '&nbsp;' . $chinese . $weekday;
}