<?php

namespace app\controllers;

class Tool
{
    /**
     * 工具箱首页
     */
    public function index()
    {
        $user_id = (int)session('user_id');

        if ($user_id) {
            $user  = model('user')->find($user_id);
            $data['user'] = $user;
        }

        $posts = model('post')->list(10);

        $data['posts'] = $posts;

        return view('tool/index', $data);
    }

    public function symbols()
    {
        return view('tool/symbols');
    }
}