<?php

namespace app\controllers;

use extend\Dir;
use Exception;

class Admin
{
    public function __construct()
    {
        if (!isAdmin()) {
            return abort(403);
        }
    }

    /**
     * 后台首页
     */
    public function index()
    {
        $user = model('user')->find((int) session('user_id'));

        $adminModel = model('admin');

        $totalMembers = $adminModel::getTotalMembers();
        $totalPosts   = $adminModel::getTotalPosts();
        $totalComments = $adminModel::getTotalComments();
        $mysqlVersion  = db('user')->mysqlVersion();

        $data = [
            'user' => $user,
            'totalMembers' => $totalMembers,
            'totalPosts'   => $totalPosts,
            'totalComments' => $totalComments,
            'mysqlVersion' => $mysqlVersion,
        ];

        return view('admin/index', $data);
    }

    /**
     * 后台设置页
     */
    public function setting()
    {
        return view('admin/setting');
    }


    /**
     * 后台网站主题页
     */
    public function theme()
    {
        $theme = config('app.theme');
        $folders = Dir::folder(VIEW_PATH);

        $data = [
            'theme' => $theme,
            'folders' => $folders['dir'],
        ];

        return view('admin/theme', $data);
    }

    /**
     * 后台用户列表页
     */
    public function users()
    {
        $totalMembers = model('admin')::getTotalMembers();
        $users = model('user')::getAll();

        $data = [
            'users' => $users,
            'totalMembers' => $totalMembers
        ];

        return view('admin/users', $data);
    }

    /**
     * 删除缓存
     */
    public function remove()
    {
        try {
            model('admin')->clearCache();

            return json('成功清空缓存', 'success');
        } catch (\Throwable $th) {
            return json('清空缓存时出错，请稍后再试。', 'error');
        }
    }

    /**
     * 根据用户 ID 获取 JSON 格式的用户信息
     */
    public function member()
    {
        $userId = (int)segment(2);

        try {
            $user = model('user')::find($userId);

            echo json_encode($user, JSON_UNESCAPED_UNICODE | JSON_INVALID_UTF8_SUBSTITUTE);
        } catch (\Throwable $th) {
            return json('获取用户信息失败', 'error');
        }
    }

    /**
     * 更新指定配置文件的配置项
     */
    public function conf()
    {
        if (REQUEST_TYPE !== 'post') {
            throw new Exception("请求方法错误。", 1);
        }

        $configItem = post('file');
        $configKey = post('key');
        $configValue = post('value');
        $configFile = ROOT_PATH . 'config/' . $configItem . '.php';

        // 首先检查文件是否存在
        if (!file_exists($configFile)) {
            return json('配置文件不存在', 'error');
        }

        try {
            $conf = config($configItem);

            // 检查配置项是否存在并更新其值
            if (array_key_exists($configKey, $conf)) {
                $configData = [$configKey => $configValue];
                write_config($configItem, $configData);

                return json('成功启用', 'success');
            } else {
                return json('指定的配置项不存在', 'error');
            }

        } catch (\Throwable $th) {
            // return json('更新配置项时发生错误', 'error');
            return json($th, 'error');
        }
    }

    /**
     * 切换
     */
    public function switch()
    {
        if (REQUEST_TYPE !== 'post') {
            throw new Exception("请求方法错误。", 1);
        }

        $configItem = post('file');
        $configKey = post('key');
        $configValue = filter_var(post('value'), FILTER_VALIDATE_BOOLEAN);
        $configFile = ROOT_PATH . 'config/' . $configItem . '.php';

        // 首先检查文件是否存在
        if (!file_exists($configFile)) {
            return json('配置文件不存在', 'error');
        }

        try {
            $conf = config($configItem);

            // 检查配置项是否存在并更新其值
            if (array_key_exists($configKey, $conf)) {
                $configData = [$configKey => $configValue];
                write_config($configItem, $configData);

                return json('成功启用', 'success');
            } else {
                return json('指定的配置项不存在', 'error');
            }

        } catch (\Throwable $th) {
            // return json('更新配置项时发生错误', 'error');
            return json($th, 'error');
        }
    }
}
