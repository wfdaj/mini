<?php

namespace app\controllers;

use core\{Captcha, Validator};

class Auth
{
    /**
     * 管理员后台添加用户
     */
    public function create()
    {
        if (!isAdmin()) {
            return json('管理员才能直接添加用户。', 'error');
            exit;
        }

        if (REQUEST_TYPE !== 'post') {
            return abort(403, '请求方法不正确');
        }

        // 获取表单数据
        $data = [
            'username' => post('username'),
            'password' => post('password'),
            'email' => post('email'),
        ];
        // 验证规则
        $rules  = [
            'username' => ['required', '3,32',  '姓名应为 3-32 个字符'],
            'password' => ['required', '6,250', '密码应为 6 个字符及以上'],
            'email'    => ['email',    '',      '请输入正确的邮箱'],
        ];
        // 验证
        $validate = new Validator($data, $rules, false);
        // 验证结果
        $result = $validate->check();
        if (!$result) {
            return json($validate->error, 'error');
        }

        $userModel = model('user');

        // 检测重名
        if ($userModel->findByUsername($data['username'])) {
            return json('用户名已存在');
        }

        // 创建用户
        $user = $userModel->create($data);
        if ($user) {
            return json('注册成功', 'success');
        }

        return json('注册失败');
    }
    /**
     * 生成验证码
     */
    public function captcha()
    {
        return (new Captcha())->make();
    }

    /**
     * 退出登录，删除全部会话数据
     * @return void
     */
    public function logout(): void
    {
        $_SESSION = [];
        session_unset();
        session_destroy();
        goBack();
        exit();
    }

    /**
     * 更改密码
     */
    public function password()
    {
        if (REQUEST_TYPE !== 'post') {
            return json('请求方法不正确', 'error');
        }

        // 获取表单数据
        $data = [
            'oldPassword'  => post('oldPassword'),
            'newPassword1' => post('newPassword1'),
            'newPassword2' => post('newPassword2'),
        ];
        // 验证规则
        $rules  = [
            'oldPassword'  => ['required', '6,32', '旧密码应为 6 个字符及以上'],
            'newPassword1' => ['required', '6,32', '新密码应为 6 个字符及以上'],
            'newPassword2' => ['same', post('newPassword2'), '重复密码与新密码不一致'],
        ];
        // 验证
        $validate = new Validator($data, $rules, true);
        // 验证结果
        $result = $validate->check();
        if (!$result) {
            return json($validate->error);
        }

        // 实例化用户模型
        $user_id = session('user_id');
        $userModel = model('user');
        $user = $userModel->find($user_id);

        // 检查旧密码
        if (!password_verify($data['oldPassword'], $user->password)) {
            return json('原密码错误');
        }

        try {
            $userModel->resetPassword($user_id, $data['newPassword1']);
            return json('修改密码成功', 'success');
        } catch (\Throwable $th) {
            return json('修改密码失败!');
        }
    }

    /**
     * 提交头像链接
     */
    public function photo()
    {
        if (REQUEST_TYPE === 'post') {
            $up = model('user')->uploadPhoto();
        }
    }
}
