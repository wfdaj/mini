<?php

namespace app\controllers;

use core\Validator;

class Login
{
    /**
     * 显示登录表单
     */
    public function index()
    {
        // 如果已有登录信息，则直接返回之前页面
        if (session('user_id')) {
            goBack();
        }

        return view('user/login');
    }

    /**
     * 提交登录
     */
    public function submit()
    {
        if (REQUEST_TYPE !== 'post') {
            return json('请求方法不正确', 'error');
        }

        // 比对验证码
        $submittedCaptcha = strtolower(post('captcha'));
        $storedCaptcha    = strtolower(session('captcha'));
        if ($submittedCaptcha !== $storedCaptcha) {
            return json('验证码错误');
        }

        // 表单数据
        $formData = [
            'username' => post('username'),
            'password' => post('password'),
        ];
        // 验证规则
        $rules  = [
            'username' => ['required', '3,32',  '姓名应为 3-32 个字符'],
            'password' => ['required', '6,30', '密码应为 6 个字符及以上'],
        ];
        // 实例化验证器
        $validate = new Validator($formData, $rules, true);
        // 验证表单输入数据是否合法
        if (false === $validate->check()) {
            return json($validate->error);
        }

        // 实例化用户模型
        $userModel = model('user');

        // 用户名存在，比对密码
        $user = $userModel->login($formData['username'], $formData['password']);

        if (!$user) {
            return json('用户名或密码错误', 'error');
        }

        // 登录成功，设置 session
        session('user_id', $user->id);
        session('username', $user->username);

        return json('登录成功', 'success');
    }
}
