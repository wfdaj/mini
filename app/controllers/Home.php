<?php

namespace app\controllers;

class Home
{
    public function index()
    {
        $user_id = (int)session('user_id');

        if ($user_id) {
            $user  = model('user')->find($user_id);
            $data['user'] = $user;
        }

        $posts = model('post')->list(10);

        $data['posts'] = $posts;

        return view('home', $data);
    }
}