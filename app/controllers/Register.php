<?php

namespace app\controllers;

use core\Validator;

class Register
{
    /**
     * 显示登录表单
     */
    public function index()
    {
        // 如果已有登录信息，则直接返回之前页面
        if (session('user_id')) {
            goBack();
        }

        return view('user/register');
    }

    public function submit()
    {
        if (REQUEST_TYPE !== 'post') {
            return abort(403, '请求方法不正确');
        }

        // 获取并检测验证码
        $captcha = post('captcha');
        if (!$captcha || strtolower($captcha) !== strtolower(session('captcha'))) {
            session_forget($captcha);  // 验证码一次性使用，使用后删除
            return json('验证码错误');
        }

        session_forget($captcha);  // 验证码验证通过，删除验证码

        // 获取表单数据
        $data = [
            'username' => post('username'),
            'password' => post('password'),
            'email' => post('email'),
        ];
        // 验证规则
        $rules  = [
            'username' => ['required', '3,32',  '姓名应为 3-32 个字符'],
            'password' => ['required', '6,250', '密码应为 6 个字符及以上'],
            'email'    => ['email',    '',      '请输入正确的邮箱'],
        ];
        // 验证
        $validate = new Validator($data, $rules, true);
        // 验证结果
        $result = $validate->check();
        if (!$result) {
            return json($validate->error);
        }

        $userModel = model('user');

        // 检测重名
        if ($userModel->findByUsername($data['username'])) {
            return json('用户名已存在');
        }

        // 创建用户
        $user = $userModel->create($data);
        if ($user) {
            // 注册成功，设置 session
            session('user_id', $user->id);
            return json('注册成功', 'success');
        }

        return json('注册失败');
    }
}