<?php

namespace app\controllers;


class Medal
{
    /**
     * 勋章首页
     */
    public function index()
    {
        $user = model('user')->find( (int) session('user_id') );
        $data['user'] = $user;

        view('medal/index', $data);
    }

    /**
     * 颁发勋章
     * 
     * award
     */
}