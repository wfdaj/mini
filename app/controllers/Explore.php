<?php

namespace app\controllers;

use core\Captcha;

class Explore
{
    /**
     * 探索频道首页
     */
    public function index()
    {
        return view('explore/index');
    }
}