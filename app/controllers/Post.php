<?php

namespace app\controllers;

use core\Validator;

class Post
{
    /**
     * 显示帖子内容
     */
    public function show()
    {
        $post_id = (int)segment(2);
        $post    = model('post')->find($post_id);

        // 查询不到帖子内容，则显示 404 页面
        if (!$post) {
            return abort(404);
        }

        $user_id  = (int)session('user_id');
        $user     = model('user')->find($user_id);

        // 记住当前地址，用于登录或注册后返回当前页
        $current_url = url('/post/show/') . $post_id;
        session('backToLink', $current_url);

        $data = [
            'post' => $post,
            'user' => $user,
            'page_title' => '帖子',
        ];

        return view('post/show', $data);
    }

    /**
     * 提交表单数据
     */
    public function submit()
    {
        if (REQUEST_TYPE !== 'post') {
            return abort(404);
        }

        // 获取表单数据
        $data['content'] = post('content');
        // 验证规则
        $rules  = [
            'content' => ['required', '1,400', '内容应为 1-200 个字符'],
        ];
        // 验证
        $validate = new Validator($data, $rules, true);
        $verify = $validate->check();

        if (!$verify) {
            return json($validate->error);
        }

        try {
            model('post')->store($data);

            return json('发帖成功', 'success');
        } catch (\Throwable $th) {
            return json('出了点小问题~');
        }
    }

    /**
     * 删除帖子
     */
    public function del()
    {
        loginCheck();

        // 从路由中获取帖子ID
        $post_id = (int)segment(2);
        // 从会话中获取当前登录用户的ID
        $currentUserId = session('user_id');

        // 尝试从数据库中获取帖子信息
        $post = model('post')->find($post_id);

        // 检查帖子是否存在以及当前用户是否是帖子的作者
        if ($post && $post->user_id === $currentUserId || isAdmin()) {
            try {
                // 删除帖子
                db('post')->where('id = ?', $post_id)->delete();

                // 返回成功信息
                return json('删除成功', 'success');
            } catch (\Throwable $th) {
                // 如果出现异常，返回错误信息
                return json('删除失败', 'error');
            }
        } else {
            // 如果帖子不存在或者当前用户不是作者，返回错误信息
            return json('您没有权限删除此帖子', 'error');
        }
    }

    /**
     * 置顶帖子
     */
    public function pin()
    {
        if (!isAdmin()) {
            return abort(403);
        }

        $postId = (int)segment(2);

        try {
            model('admin')->toggleStickyStatus($postId);

            // 返回成功信息
            return json('成功', 'success');
        } catch (\Throwable $th) {
            // 如果出现异常，返回错误信息
            return json('失败', 'error');
        }
    }

}
