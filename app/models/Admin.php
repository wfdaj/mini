<?php

namespace app\models;

use core\Model;
use Exception;

class Admin extends Model
{
    /**
     * 缓存 获取会员总数
     */
    // public function getTotalMembers()
    // {
    //     return $this->cache('adminTotalMembers', '', '__getTotalMembers', 10);
    // }

    /**
     * 直接 获取会员总数
     */
    public static function getTotalMembers()
    {
        return db('user')->count();
    }

    /**
     * 直接 获取帖子总数
     */
    public static function getTotalPosts()
    {
        return db('post')->count();
    }

    /**
     * 直接 获取回复总数
     */
    public static function getTotalComments()
    {
        return db('comment')->count();
    }

    /**
     * 直接 获取回复总数
     */
    public static function getMysqlVersion()
    {
        return db('comment')->mysqlVersion();
    }

    /**
     * 删除缓存
     *
     * @throws \Exception 如果获取缓存对象失败或清除缓存时出错
     */
    public function clearCache()
    {
        // 获取缓存对象
        $this->getCacher();

        // 检查缓存对象是否有效
        if (!$this->cacher) {
            throw new Exception("无法获取有效的缓存对象。");
        }

        // 尝试清除缓存，并处理可能出现的异常
        try {
            $this->cacher->clearCache();
        } catch (Exception $e) {
            // 记录异常信息或进行其他适当的错误处理
            throw new Exception("清除缓存时出错：" . $e->getMessage(), 0, $e);
        }
    }

    /**
     * 切换帖子的置顶状态
     *
     * @param int $postId 帖子ID
     * @return bool 操作结果
     * @throws Exception 如果出现异常则抛出
     */
    public function toggleStickyStatus(int $postId)
    {
        $postDB = db('post');

        // 查询帖子及其置顶状态
        $post = $postDB->where('id = ?', $postId)->first();

        if (!$post) {
            throw new Exception("帖子不存在");
        }

        try {
            $newStickyStatus = $post->is_sticky ? 0 : 1;
            $postDB->where('id = ?', $postId)->update(['is_sticky' => $newStickyStatus]);

            return true;
        } catch (\PDOException $e) {
            throw new Exception("置顶帖子时数据库操作出错：" . $e->getMessage());
        } catch (\Exception $e) {
            throw new Exception("置顶帖子时发生异常：" . $e->getMessage());
        }
    }
}
