<?php

namespace app\models;

use core\Model;

class Reward extends Model
{
    /**
     * 根据用户 id 查询用户是否已签到
     */
    public function checkIfSigned(int $user_id)
    {
        $reward = db('reward');

        $fields = 'r.*, u.golds';
        return $reward->join('AS r LEFT JOIN user AS u ON r.user_id = u.id')
                              ->where('r.user_id = ?', [$user_id])
                              ->first($fields);

    }

    /**
     * 计算签到时间差是否大于24小时
     */
    public function timeDiff(int $user_id): int
    {
        $signed = $this->checkIfSigned($user_id);

        // 有签到信息
        if ($signed) {
            $last_sign_time = time() - (int)$signed->updated_at;
            $time_diff      = abs(round($last_sign_time / 3600));

            return $time_diff;
        } else {
            return 0;
        }
    }

    /**
     * 首次签到，保存数据并关联用户表金币加 6
     */
    public function store(array $data)
    {
        $reward = db('reward');

        // 开启事务
        $reward->beginTransaction();

        try {
            // 插入签到数据
            $reward->where('user_id = ?', [$data['user_id']])->insert($data);
            // 金币增加 3 个
            db('user')->where('id = ?', [$data['user_id']])->increment('golds', 3);
            // 提交事务
            $reward->commit();

            return true;
        } catch (\Throwable $th) {
            // 回滚
            return $reward->rollback();
        }
    }

    /**
     * 已签到过，只更新签到时间
     */
    public function update(array $data)
    {
        $reward = db('reward');

        $reward->beginTransaction();

        try {
            $reward->where('user_id = ?', [$data['user_id']])->update($data);
            db('user')->where('id = ?', [$data['user_id']])->increment('golds', 3);
            $reward->commit();

            return true;
        } catch (\Throwable $th) {
            return $reward->rollback();
        }
    }

    /**
     * 查询今日签到人员列表
     */
    public function list()
    {
        $reward = db('reward');

        $current_time = time()-86400;
        return $reward->where('updated_at >= ?', $current_time)->get();
    }
}
