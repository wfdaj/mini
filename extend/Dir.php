<?php

namespace extend;

use Exception;

class Dir
{
    public static function folder($dir) {
        // Normalize and validate directory path
        $dir = rtrim($dir, '/') . '/';
        if (!is_readable($dir)) {
            throw new Exception("Directory '{$dir}' is not readable.");
        }

        $dirArray = [
            'dir'  => [],
            'file' => []
        ];

        // Use scandir to list files and directories
        $entries = scandir($dir);
        foreach ($entries as $entry) {
            if ($entry != '.' && $entry != '..') { // Skip '.' and '..'
                $fullPath = $dir . $entry;
                if (is_dir($fullPath)) {
                    $dirArray['dir'][] = $entry;
                } else {
                    $dirArray['file'][] = $entry;
                }
            }
        }

        return $dirArray;
    }
}
