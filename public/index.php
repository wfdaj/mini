<?php

declare(strict_types=1);

/**
 * 项目入口文件
 *
 * 更新时间: 2024-4-28
 *
 */

// 内容类型和编码
header('Content-Type: text/html; charset=utf-8');
// 仅发送文件的源作为引用地址
header("Referrer-Policy: origin");
// 禁用客户端的 MIME 类型嗅探行为
header("X-Content-Type-Options: nosniff");
// 防止被站外加入 iframe 中浏览
header("X-Frame-Options: DENY");
// 启用 XSS 过滤
header("X-XSS-Protection: 1; mode=block");
header('x-powered-by:MINI');

// 框架核心函数
require __DIR__ . '/../core/functions.php';

// 启动框架
new \core\App();