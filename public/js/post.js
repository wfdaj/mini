document.addEventListener("DOMContentLoaded", function () {
    // 点赞
    const jLikes = document.querySelectorAll('.js-like');

    jLikes.forEach(jLike => {
        jLike.addEventListener('click', function () {
            const pid = this.getAttribute('data-pid');
            const likesNumSelector = `.js-likesNum${pid}`;
            const likesNumElement = document.querySelector(likesNumSelector);
            const postActionLikeElement = this; // 假设按钮本身就是 post-action-like
            const iconHeartElement = this.querySelector('.icon-heart');
            const iconHeartFillElement = this.querySelector('.icon-heart-fill');
            let numTextContent = likesNumElement.textContent.trim();
            let num = 0;

            if (numTextContent !== '') {
                num = parseInt(numTextContent, 10); // 尝试将非空文本转换为整数
            }

            fetch(`/like/love/${pid}`)
                .then(response => response.json())
                .then(res => {
                    if (res.status === 'success') {
                        if (jLike.classList.contains('liked')) {
                            num--;
                            postActionLikeElement.setAttribute('data-bs-original-title', '喜欢');
                            iconHeartFillElement.classList.remove('icon-heart-fill');
                            iconHeartFillElement.classList.add('icon-heart');
                        } else {
                            num++;
                            postActionLikeElement.setAttribute('data-bs-original-title', '取消喜欢');
                            iconHeartElement.classList.remove('icon-heart');
                            iconHeartElement.classList.add('icon-heart-fill');
                        }
                        jLike.classList.toggle('liked');
                        likesNumElement.textContent = num;
                    } else {
                        toast.error(res.message);
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        });
    });

    // 收藏
    var jFavs = document.querySelectorAll('.js-fav');

    jFavs.forEach(jFav => {
        jFav.addEventListener('click', function () {
            let pid = jFav.getAttribute("data-pid");
            var favNumSelector = `.js-favNum${pid}`;
            var favNumElement = jFav.parentElement.querySelector(favNumSelector);
            var postActionFavElement = jFav.parentElement.querySelector('.post-action-fav'); // 关联到当前按钮的父元素或其他适当的选择器
            var iconFavFillElement = jFav.parentElement.querySelector('.icon-fav-fill'); // 同上
            var iconFavElement = jFav.parentElement.querySelector('.icon-fav'); // 同上

            let numTextContent = favNumElement.textContent.trim();
            let num = 0;

            if (numTextContent !== '') {
                num = parseInt(numTextContent, 10); // 尝试将非空文本转换为整数
            }

            fetch(`/fav/add/${pid}`)
                .then((response) => response.json())
                .then((res) => {
                    if (res.status === 'success') {
                        if (jFav.classList.contains("favorited")) {
                            num--;
                            postActionFavElement.setAttribute('data-bs-original-title', '收藏');
                            iconFavFillElement.classList.remove('icon-fav-fill');
                            iconFavFillElement.classList.add('icon-fav');
                        } else {
                            num++;
                            postActionFavElement.setAttribute('data-bs-original-title', '取消收藏');
                            iconFavElement.classList.remove('icon-fav');
                            iconFavElement.classList.add('icon-fav-fill');
                        }

                        jFav.classList.toggle('favorited');
                        favNumElement.textContent = num;
                    } else {
                        toast.error(res.message);
                    }
                });
        });
    });

    // 删除帖子
    // 绑定点击事件到document上，通过事件冒泡来捕获动态生成的元素的事件
    document.addEventListener('click', function (event) {
        // 检查被点击的元素是否有'jsDelBtn'这个ID
        if (event.target.id === 'jsDelBtn') {
            event.preventDefault();

            // 获取被点击的按钮
            var button = event.target;
            var postId = button.dataset['postId'];

            if (!postId) {
                toast.error('未提供帖子ID');
                return; // 如果没有提供帖子ID，则退出函数
            }

            // 禁用删除按钮并显示加载指示器
            button.disabled = true;
            button.innerText = '删除中...';

            fetch(`/post/del/${postId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ post_id: postId })
            })
                .then(response => response.json())
                .then(res => {
                    if (res.status === 'success') {
                        toast.success(res.message);
                        setTimeout(function () {
                            window.location.href = "/";
                        }, 2000);
                    } else {
                        toast.error(res.message);
                    }
                    // 重新启用删除按钮并恢复原始文本
                    button.disabled = false;
                    button.innerText = '确认删除';
                })
                .catch(error => {
                    console.error('Fetch Error:', error);
                    // 重新启用删除按钮并恢复原始文本
                    button.disabled = false;
                    button.innerText = '确认删除';
                });
        }
    });

    // 置顶帖子
    const jPin = document.getElementById("js-pin");

    jPin?.addEventListener('click', function (event) {
        event.preventDefault();
        // 获取被点击的按钮
        var button = event.target;
        var postId = button.dataset['postId'];

        if (!postId) {
            toast.error('未提供帖子ID');
            return; // 如果没有提供帖子ID，则退出函数
        }

        fetch(`/post/pin/${postId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ post_id: postId })
        })
            .then(response => response.json())
            .then(res => {
                if (res.status === 'success') {
                    toast.success(res.message);
                } else {
                    toast.error(res.message);
                }
                // 重新启用删除按钮并恢复原始文本
                button.disabled = false;
            })
            .catch(error => {
                console.error('Fetch Error:', error);
                // 重新启用删除按钮并恢复原始文本
                button.disabled = false;
            });
    });

    // 未登录跳转
    const jsJump = document.querySelectorAll('.js-jump');
    if (jsJump) {
        jsJump.forEach(function (element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();

                toast.info('正在跳转至登录页面...');
                setTimeout(function () {
                    location.href = "/login";
                }, 1500);
            });
        });
    }
});
