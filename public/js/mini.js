document.addEventListener("DOMContentLoaded", () => {
    // 下滑隐藏，上滑显示
    if (document.querySelector('.js-autohide')) {
        var lastScrollTop = 0;
        window.addEventListener('scroll', function () {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            var direction = scrollTop < lastScrollTop ? 'up' : 'down';
            document.querySelectorAll('.js-autohide').forEach(el => {
                el.classList.remove(`scrolled-${direction === 'up' ? 'down' : 'up'}`);
                el.classList.add(`scrolled-${direction}`);
            });
            lastScrollTop = scrollTop;
        });
    }

    // 点击不关闭下拉菜单
    document.body.addEventListener('click', function (e) {
        if (e.target.matches('[data-stopPropagation]')) {
            e.stopPropagation();
        }
    });

    // 刷新页面，只为第一个.js-refresh元素添加事件监听器
    let jRefresh = document.querySelector('.js-refresh');
    if (jRefresh) {
        jRefresh.addEventListener('click', function () {
            window.location.reload();
        });
    }

    // 后退, 没有来源页面信息的时候, 改成首页URL地址
    let jBack = document.querySelector('.js-back');
    if (jBack) {
        jBack.addEventListener('click', function () {
            if (document.referrer === "") {
                window.location.href = "/";
            } else {
                window.history.back();
            }
        });
    }

    // 回到页面顶部
    document.querySelectorAll('.js-top').forEach((element) => {
        element.addEventListener('click', function (event) {
            event.preventDefault();
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
            return false;
        });
    });

    // 点击刷新验证码
    const resetCaptchaElement = document.getElementById('resetCaptcha');
    if (resetCaptchaElement) {
        resetCaptchaElement.addEventListener('click', function () {
            this.src = '/auth/captcha?' + Math.random();
        });
    }


    // 点击响应整行
    // document.querySelectorAll('.js-tap').forEach(function (element) {
    //     element.addEventListener('click', function (e) {
    //         var href = this.getAttribute('href') || this.getAttribute('data-href');
    //         // 图片不响应
    //         if (e.target.nodeName === 'IMG') return true;
    //         // 操作按钮不响应
    //         if (e.target.nodeName === 'I') return true;
    //         // GIF动画不响应
    //         if (e.target.nodeName === 'CANVAS') return true;
    //         if (e.ctrlKey) {
    //             window.open(href);
    //             return false;
    //         } else {
    //             window.location = href;
    //         }
    //     });
    // });
    document.querySelectorAll('.js-tap').forEach(function (container) {
        container.addEventListener('click', function (e) {
            // 检查事件的目标是否包含特定的类名，比如 'no-click'
            if (e.target.classList.contains('js-no-click')) {
                return; // 如果包含，则不执行后续代码
            }

            // // 检查事件的目标是否是容器本身，而不是其子元素
            // if (e.target !== container) {
            //     return; // 如果不是容器本身，则不执行后续代码
            // }

            var href = container.getAttribute('href') || container.getAttribute('data-href');
            if (!href) return; // 如果没有href或data-href，则不执行后续代码

            if (e.ctrlKey) {
                window.open(href, '_blank'); // 打开新窗口或标签页
            } else {
                window.location = href; // 在当前窗口或标签页中导航
            }
        });
    });

    /**
     * 初始化提示框 bootstrap tooltips
     */
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

    /**
     * 点击激活提交按钮
     */
    // function toggleSubmitButton(enable) {
    //     var submitButton = document.getElementById("submitButton");
    //     submitButton.disabled = !enable;
    // }
    // function activateButton(e) {
    //     toggleSubmitButton(e.checked);
    // }

    /**
     * 点击图片放大
     */
    // document.querySelectorAll('.feed-item-img').forEach((img) => {
    //     img.addEventListener('click', toggleImageSize);
    //     img.style.cursor = "zoom-in";
    // });

    // function toggleImageSize(event) {
    //     event.preventDefault();
    //     const img = event.target;
    //     const currentSrc = img.src;

    //     if (img.style.maxHeight) {
    //         img.style.maxHeight = ''; // 重置为初始值
    //     } else {
    //         img.style.maxHeight = '1000px'; // 展开到足够大的高度值
    //         img.style.cursor = "zoom-out";
    //     }

    //     // 获取大图的URL，它存储在data-fullsize属性中
    //     const fullSizeUrl = img.getAttribute('data-fullsize');

    //     // 检查当前显示的图片是否为大图
    //     if (img.src === fullSizeUrl) {
    //         // 如果当前是大图，则切换回缩略图
    //         img.src = currentSrc; // 假设在之前某处设置了data-original为缩略图的URL
    //         // 如果之前没有设置data-original，你可能需要在页面加载时或通过其他方式存储这个值
    //     } else {
    //         // 如果当前不是大图，则切换到大图
    //         img.src = fullSizeUrl;
    //         // 可选：存储当前缩略图的URL，以便之后可以切换回来
    //         img.setAttribute('data-original', img.getAttribute('src'));
    //     }

    //     // 滚动到图片位置
    //     // img.scrollIntoView({ behavior: 'smooth' });
    //     window.scrollTo({
    //         top: img.offsetTop - 60,
    //         behavior: 'smooth'
    //     });
    // }

    // 遍历每一个 textarea 自适应高度
    const jTextarea = document.querySelectorAll('.js-textarea');
    if (jTextarea) {
        jTextarea.forEach(function (e) {
            // 为其添加 input 事件监听器
            e.addEventListener('input', function () {
                // 将 textarea 的高度设置为 auto，以便它可以根据内容自动调整大小
                this.style.height = 'auto';
                // 计算 textarea 的 scrollHeight，这将返回内容所需的最小高度
                this.style.height = `${this.scrollHeight}px`;
            });
            e.addEventListener('keyup', function () {
                let content = this.value.length;
                if (content > 0 && content <= 140) {
                    // 选择所有包含 .submitButton 类名的元素并使它们可用
                    document.querySelectorAll('.submitButton').forEach(function (button) {
                        button.disabled = false;
                    });
                } else {
                    // 选择所有包含 .submitButton 类名的元素并禁用它们
                    document.querySelectorAll('.submitButton').forEach(function (button) {
                        button.disabled = true;
                    });
                }
            });
        });
    }
});



/**
 * 高亮当前链接
 * @param {string} activeLink - 需要高亮的链接对应的 data-active 值
 */
function highlightActiveLink(activeLink) {
    // 选择具有特定 data-active 值的 <a> 元素
    let element = document.querySelector(`a[data-active="${activeLink}"]`);

    // 检查元素是否存在
    if (element) {
        // 添加 'active' 类名，同时确保不会与现有的类名粘连
        element.className = (element.className ? element.className + ' ' : '') + 'active';
    }
}

/**
 * 计算字符长度（中文算2个字节）
 */
const getStringLength = (str) => {
    let realLength = 0;
    for (let i = 0; i < str.length; i++) {
        const charCode = str.charCodeAt(i);
        realLength += (charCode >= 0x4E00 && charCode <= 0x9FFF) ? 2 : 1;
    }
    return realLength;
};

/**
 * toast
 */
const toast = {
    // 默认的显示时长
    duration: 3000,

    // 成功提示
    info(message, duration = toast.duration) {
        toast.show(message, '', duration);
    },

    // 成功提示
    success(message, duration = toast.duration) {
        toast.show(message, 'toast-success', duration);
    },

    // 错误提示
    error(message, duration = toast.duration) {
        toast.show(message, 'toast-error', duration);
    },

    // 显示提示的核心方法
    show(message, type = 'default', duration = this.duration) {
        const toastContainer = document.createElement('div');
        toastContainer.classList.add('toast');

        // 检查 type 是否有效，如果无效则使用默认值 'default'
        if (type && typeof type === 'string' && type.trim() !== '') {
            toastContainer.classList.add(type);
        } else {
            // 可选：如果想要一个默认的样式类，可以取消下一行的注释
            toastContainer.classList.add('default');
        }

        toastContainer.textContent = message;

        document.body.appendChild(toastContainer);

        setTimeout(() => {
            toastContainer.classList.add('show');
        }, 0);

        setTimeout(() => {
            toastContainer.classList.remove('show');
            setTimeout(() => {
                document.body.removeChild(toastContainer);
            }, 300);
        }, duration || this.duration);
    }
};