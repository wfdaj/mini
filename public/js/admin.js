$(document).ready(function () {
    // 添加会员
    const addUserBtn = $('#addUserBtn');

    addUserBtn.on('click', (event) => {
        event.preventDefault();
        addUserBtn.prop('disabled', true); // 禁用提交按钮，防止重复提交

        $.ajax({
            method: 'POST',
            url: '/auth/create',
            data: new FormData($('#addUserForm')[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (res) {
                addUserBtn.prop('disabled', false); // 重新启用提交按钮
                if (res.status === 'success') {
                    toast.success(res.message);
                    setTimeout(() => { window.location.reload(); }, 2000); // 刷新当前页
                } else {
                    toast.error(res.message); // 显示错误消息
                }
            },
            error: function (xhr) {
                addUserBtn.prop('disabled', false); // 重新启用提交按钮
                toast.error(xhr.responseJSON?.message || '发生错误，请稍后再试。');
            }
        });
    });

    // 清空缓存
    const clearCacheBtn = $('#clearCacheBtn');

    clearCacheBtn.on('click', (event) => {
        event.preventDefault();

        $.ajax({
            method: 'POST',
            url: '/admin/remove',
            dataType: 'json',
            success: function (res) {
                if (res.status === 'success') {
                    toast.success(res.message);
                } else {
                    toast.error(res.message);
                }
            },
            error: function (xhr) {
                toast.error(xhr.responseJSON?.message || '发生错误，请稍后再试。');
            }
        });
    });

    // 切换配置项
    $('#switchStates').on('change', function() {
        var isChecked = $(this).is(':checked');
        $.ajax({
            url: '/admin/toggleConfig.php', // 后端处理脚本的URL
            type: 'POST',
            data: { toggle: isChecked ? 'on' : 'off' }, // 发送checkbox的状态
            success: function(res) {
                toast.success(res.message);
            },
            error: function(xhr, status, error) {
                toast.error('有点小错误。');
                // console.error("AJAX Error: " + error);
            }
        });
    });
});