<?php

/**
 * 数据库 配置文件
 */

return [
    'driver'   => 'mysql',
    'host'     => env('host') ?: '127.0.0.1',
    'database' => env('database') ?: 'mini_test',
    'username' => env('username') ?: 'mini_test',
    'password' => env('password') ?: 'HspSR5DLdemhekdw',
    'charset'  => env('charset') ?: 'utf8mb4',
    'port'     => env('port') ?: 3306,
    'prefix'   => env('prefix') ?: '',
];