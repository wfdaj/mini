<?php

/**
 * 缓存 配置文件
 * 
 * 缓存类型
 * 'allowCacheType' => ['file', 'memcache', 'redis'],
 */

return [
    'start'    => true,
    'allowCacheType' => ['file', 'memcache', 'redis'],
    'driver'   => 'file',
    'path'     => CACHE_PATH,
    // 主机地址 [ 'memcache', 'redis' 需要设置 ]
    'host'     => '127.0.0.1',
    // 对应各类服务的密码, 为空代表不需要密码
    'password' => '',
    // 对应服务的端口
    'port'     => '6379',
    'prefix'   => 'cache_'
];