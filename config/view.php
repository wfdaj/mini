<?php

/**
 * 模板 配置文件
 */

return [
    'view_path'   => VIEW_PATH . config('app.theme') . '/',
    'cache_path'  => CACHE_PATH,
    'view_suffix' => 'php',
];