<?php

/**
 * 附件上传 配置文件
 */

return [
    'mimes'        => ['image/png', 'image/jpeg', 'image/pjpeg', 'image/x-png', 'image/gif'],   //允许上传的文件MiMe类型
    'maxSize'      => 3,                       // 上传的文件大小限制 MB (0-不做限制)
    'exts'         => ['jpg', 'jpeg', 'gif', 'png'],   // 允许上传的文件后缀
    'autoSub'      => true,                    // 自动子目录保存文件
    'subName'      => ['date', 'Y/m/d'],       // 子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
    'rootPath'     => 'uploads/',              // 保存根路径
    'savePath'     => '',                      // 保存路径
    'saveName'     => ['uuid', 32],            // 上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
    'saveExt'      => '',                      // 文件保存后缀，空则使用原后缀
    'replace'      => true,                   // 存在同名是否覆盖
    'hash'         => false,                   // 是否生成hash编码
    'callback'     => false,                   // 检测文件是否存在回调，如果存在返回文件信息数组
    'driver'       => 'local',                 // 文件上传驱动
    'driverConfig' => [],                      // 上传驱动配置
];