<?php
return array (
  'debug' => true,
  'trace' => false,
  'timezone' => 'Asia/Shanghai',
  'not_allowed_join' => false,
  'maintenance' => false,
  'installed' => 1,
  'name' => '迷你博客 | 心情日记 | 灵感记录 | 即时新闻',
  'theme' => 'twitter',
);
