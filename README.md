# mini

#### 介绍
PHP + MySQL 仿 twitter 程序

#### 演示截图
![程序截图](程序截图.png)


#### 安装教程

1.  修改数据库配置文件 config/db.php
2.  配置文件 config/app.php 中的 `debug` 和 `trace` 只在调试时开启。

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
